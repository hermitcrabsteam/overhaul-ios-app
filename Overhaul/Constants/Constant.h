//
//  Constant.h
//  Overhaul
//
//  Created by admin on 15/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#import "AppDelegate.h"
#import "ViewController.h"
#import "SignUpViewController.h"
#import "OTPViewController.h"
#import "AddVehicleViewController.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "ForgotPwdViewController.h"
#import "ServiceListViewController.h"
#import "MerchantViewController.h"
#import "MyProfileViewController.h"
#import "NotificationViewController.h"
#import "FilterViewController.h"
#import "NotificationViewController.h"
#import "ReviewsViewController.h"
#import "NewsUpdatesViewController.h"
#import "ContactUsViewController.h"
#import "RateUsViewController.h"
#import "AboutUsViewController.h"
#import "SettingsViewController.h"
#import "FavouritesViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>

#import "MBProgressHUD.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "DHValidation.h"

#define IS_IPhone4_HEIGHT_GTE_480 ([[UIScreen mainScreen ] bounds].size.height == 480.0f)
#define IS_IPhone5_HEIGHT_GTE_568 ([[UIScreen mainScreen ] bounds].size.height == 568.0f)
#define IS_IPhone6_HEIGHT_GTE_667 ([[UIScreen mainScreen ] bounds].size.height == 667.0f)
#define IS_IPhone6Plus_HEIGHT_GTE_736 ([[UIScreen mainScreen ] bounds].size.height == 736.0f)

#define FACEBOOK_CLIENT_ID [NSString stringWithFormat:@"fb%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"]]

#define GPLUS_CLIENT_ID @"63819620133-llukkj5tv633pp14jhu6sc14nsr03a0t.apps.googleusercontent.com";

#define kAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate]);

#define BaseURL @"http://ec2-52-38-149-137.us-west-2.compute.amazonaws.com:8080/overhaul"

#define UrlMasterData @"/masterData"
#define UrlLogin @"/userLogin"
#define UrlMerchantLogin @"/merchantLogin"   // why we need this when we have Login
#define UrlRegisterUser @"/registerUser"
#define UrlMerchantSignUp @"/merchantSignup"
#define UrlResendUserOTP @"/resendOTP"
#define UrlVerifyUser @"/verifyUser"
#define UrlForgotPwd @"/forgotPassword"
#define UrlResendMerchantOTP @"/resendOTPToMerchant"
#define UrlSetUserPassword @"/setPassword"
#define UrlSetMerchantPassword @"/setPasswordForMerchat"
#define UrlServiceStationById @"/getServiceStationById"
#define UrlAddServiceStation @"/addServiceStation"
#define UrlAddVehicle @"/addVehicle"
#define UrlGetFavourites @"/getFavourites"
#define UrlGetAllVehicles @"/getVehicles"
#define UrlGetVehiclesByID @"/getVehiclesByUserId"
#define UrlRecentStations @"/recentStations"
#define UrlGetReplyByReview @"/getReplysByReview"
#define UrlGetReviewByUser @"/getReviewsByUser"
#define UrlGetReviewByStation @"/getReviewsByStation"
#define UrlAddRecentlyViewedStations @"/addRecentlyViewed"
#define UrlAddReply @"/addReply"
#define UrlAddReview @"/addReview"
#define UrlUpdateServiceStation @"/updateServiceStation"
#define UrlAddFavourite @"/addFavourite"
#define UrlGetStations @"/getStations"
#define UrlGetRepairsStations @"/getStationsForRepairs"
#define UrlGetFuelStations @"/getNearByBunks"
#define UrlGetSpareParts @"/getNearBySparePartsOutlets"
#define UrlGetAccessories @"/getNearByAccessoryOutlets"
#define UrlGetPerformanceUpgrade @"/getStationsForUpgrades"
#define UrlGetCarWash @"/getNearByCarWashOutlets"
#define UrlGetTyres @"/getNearByTyreOutlets"

#endif /* Constant_h */
