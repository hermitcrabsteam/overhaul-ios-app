//
//  WebViewController.m
//  BurrnSample
//
//  Created by Jobin on 11/21/14.
//  Copyright (c) 2014 adaptiz. All rights reserved.
//

#import "WebCommunicationHandler.h"

@implementation WebCommunicationHandler

@synthesize downloadDelegate = _downloadDelegate;

- (void) downloadWithURL : (NSString *)requestURLString
                   params: (id)params
        WithRequestMethod:(NSString *)requestmethod
                authorize: (BOOL) authorize
          networkHandler : (id<WebCommunicationHandlerDelegate>)delegate
                 context : (NSString *) context {
    
    self.downloadDelegate = delegate;
    contextString = context;
    NSLog(@"params :%@",params);
    NSURL *requestUrl = [[NSURL alloc] init];
    
    if (requestURLString != nil)
        requestUrl = [NSURL URLWithString:requestURLString];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:requestUrl];
    
    /*
     if(authorize)
     {
     NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:kUserInfo];
     NSString *token = [userDict valueForKey:@"u_token"];
     [httpClient setDefaultHeader:@"ttl_token" value:token];
     [httpClient setDefaultHeader:@"application/json" value:@"Accept"];
     [httpClient setDefaultHeader:@"ttl_key" value:@"xxxx"];
     }
     else
     {
     [httpClient setDefaultHeader:@"ttl_key" value:@"xxxx"];
     }
     */
    
    httpClient.parameterEncoding = AFJSONParameterEncoding;
    
    id parameters = nil;
    
    if ([params isKindOfClass:[NSDictionary class]])
        parameters = [NSDictionary dictionaryWithDictionary:params];
    else if ([params isKindOfClass:[NSArray class]])
        parameters = [NSArray arrayWithArray:params];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:requestmethod path:nil parameters:parameters];
    
    request.timeoutInterval = 15;
    
    //    [AFJSONRequestOperation addAcceptableContentTypes:
    //     [NSSet setWithObject:@"text/plain"]];
    
    //    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    //    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    //
    //    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    //    NSLog(@"Header = %@",request.allHTTPHeaderFields);
    //    NSDictionary *allHeadFields = nil;
    //
    //    [request setAllHTTPHeaderFields:allHeadFields];
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSLog(@"request:%@",request);
    if (request)
        [self fetchJsonResponseWithRequest:request];
    
}

- (void) uploadFiletoURL : (NSURL *)url
                   params: (id)params
                 fileData: (NSData*)videoData
                authorize: (BOOL) authorize
          networkHandler : (id<WebCommunicationHandlerDelegate>)delegate
                 context : (NSString *) context {
    
}

- (void) fetchJsonResponseWithRequest : (NSMutableURLRequest *) request {
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             [self.downloadDelegate communicationHandler:self downloadDidCompleteWithResponse:response  error:nil responseObject:JSON context:contextString];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             
                                             NSInteger code = error.code;
                                             if (code == NSURLErrorUnsupportedURL ||
                                                 code == NSURLErrorTimedOut ||
                                                 code == NSURLErrorNetworkConnectionLost||
                                                 code == NSURLErrorBadURL ||
                                                 code == NSURLErrorBadServerResponse ||
                                                 code == NSURLErrorRedirectToNonExistentLocation ||
                                                 code == NSURLErrorFileDoesNotExist ||
                                                 code == NSURLErrorFileIsDirectory ||
                                                 code == NSURLErrorRedirectToNonExistentLocation){
                                                 
                                               /*  UIAlertView * alert = [[UIAlertView alloc] initWithTitle: KAppName
                                                                                                  message: @"Network error,Please try again later."
                                                                                                 delegate:nil cancelButtonTitle:  nil
                                                                                        otherButtonTitles:@"OK",nil];
                                                 [alert show];*/
                                                 
                                                 [self.downloadDelegate communicationHandler:self downloadDidCompleteWithResponse:response error:error responseObject:nil context:contextString];
                                                 return ;
                                                 
                                             }
                                             
                                             [self.downloadDelegate communicationHandler:self downloadDidCompleteWithResponse:response error:error responseObject:nil context:contextString];
                                         }];
    
    [operation start];
    
    
}

@end
