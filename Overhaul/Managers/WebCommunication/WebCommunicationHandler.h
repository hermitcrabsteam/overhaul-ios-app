//
//  WebViewController.h
//  BurrnSample
//
//  Created by Jobin on 11/21/14.
//  Copyright (c) 2014 adaptiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@protocol WebCommunicationHandlerDelegate;

@interface WebCommunicationHandler: NSObject {
    
    NSString *contextString;
}


@property (weak , nonatomic)id<WebCommunicationHandlerDelegate> downloadDelegate;

- (void) downloadWithURL : (NSString *)requestURLString
                   params: (id)params
        WithRequestMethod:(NSString *)requestmethod
                authorize: (BOOL) authorize
          networkHandler : (id<WebCommunicationHandlerDelegate>)delegate
                 context : (NSString *) context;


@end

@protocol WebCommunicationHandlerDelegate
- (void) communicationHandler  : (WebCommunicationHandler *)handler
downloadDidCompleteWithResponse: (NSHTTPURLResponse *) serverResponse
                         error : (NSError *) error
               responseObject  : (id) responseObject
                       context : (NSString *) context;
@end