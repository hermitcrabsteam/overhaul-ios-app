//
//  AppDelegate.m
//  Overhaul
//
//  Created by Adaptiz on 22/06/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    navController = (UINavigationController *)self.window.rootViewController;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
   //hemanth
//    NSError* configureError;
//    [[GGLContext sharedInstance] configureWithError: &configureError];
//    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}



- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options
{
    NSString *urlString = url.absoluteString;
    
    if([urlString rangeOfString:FACEBOOK_CLIENT_ID].location != NSNotFound)
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                              openURL:url
                                                    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                           annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
        
        return YES;
    }
    
    
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}




- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    NSString *urlString = url.absoluteString;
    if([urlString rangeOfString:FACEBOOK_CLIENT_ID].location != NSNotFound)
    {
        
    }
    return YES;
}



- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    NSString *urlString = url.absoluteString;
    
    if([urlString rangeOfString:FACEBOOK_CLIENT_ID].location != NSNotFound)
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation
                ];
        
        
    }
    else
    {
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
    }
    
    return YES;
}




- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error
{
    if (error)
    {
        NSLog(@"error:%@",error);
    }
    else
    {
        NSMutableDictionary *dictGPlus=[[NSMutableDictionary alloc]init];
        
        [dictGPlus setValue:user.userID forKey:@"userID"];
        [dictGPlus setValue:user.profile.name forKey:@"name"];
        [dictGPlus setValue:user.profile.email forKey:@"email"];
        
        if ([GIDSignIn sharedInstance].currentUser.profile.hasImage)
        {
            NSUInteger dimension = round(1000 * [UIScreen mainScreen].scale);
            NSURL *imageURL = [user.profile imageURLWithDimension:dimension];
            [dictGPlus setValue:imageURL forKey:@"imageURl"];
        }
        
        NSLog(@"Google User Details -----> %@", dictGPlus);
        
      //  self.customerID = [dictGPlus objectForKey:@""];
        self.socialLoginDetails = dictGPlus;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"GooglePlusData" object:nil];
        
    }
}


- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    // Perform any operations when the user disconnects from app here.
    // ...
}


-(void)showAlertViewWithMessage:(NSString *)message WithTitle:(NSString *)title cancelButton:(NSString *)cancelButton
{
    float iOSVersion = ([UIDevice currentDevice].systemVersion).floatValue;
    
    if (iOSVersion < 8.0f)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle: cancelButton otherButtonTitles: nil];
        
        [alert show];
    }
    else
    {
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* noButton = [UIAlertAction actionWithTitle:cancelButton style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                   {
                                       [rootViewController dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
        
        [alert addAction:noButton];
        
        [rootViewController presentViewController:alert animated:YES completion:nil];
    }
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
