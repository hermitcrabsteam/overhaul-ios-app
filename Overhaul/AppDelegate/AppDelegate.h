//
//  AppDelegate.h
//  Overhaul
//
//  Created by Adaptiz on 22/06/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "Constant.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate>
{
    UINavigationController *navController;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray *responseDict;
@property (strong, nonatomic) NSMutableArray *carsResponseArray, *bikesResponseArray;
@property (strong, nonatomic) NSString *sessionID, *customerID, *isLoginFrom, *specialPrice, *userID;
@property (strong, nonatomic) NSDictionary *socialLoginDetails;

-(void)showAlertViewWithMessage:(NSString *)message WithTitle:(NSString *)title cancelButton:(NSString *)cancelButton;

@end

