//
//  MerchantFooterTableViewCell.h
//  Overhaul
//
//  Created by Apple on 8/17/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MerchantFooterTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *callNowButton;
@property (strong, nonatomic) IBOutlet UIButton *bookAppointmentButton;

@end
