//
//  MenuListTableViewCell.h
//  Overhaul
//
//  Created by admin on 14/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *menuListIconImageView;
@property (strong, nonatomic) IBOutlet UILabel *menuListNameLabel;

@end
