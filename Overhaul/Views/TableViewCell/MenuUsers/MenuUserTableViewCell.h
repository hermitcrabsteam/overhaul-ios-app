//
//  MenuUserTableViewCell.h
//  Overhaul
//
//  Created by admin on 14/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuUserTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *userImageView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *userMobileLabel;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;


@end
