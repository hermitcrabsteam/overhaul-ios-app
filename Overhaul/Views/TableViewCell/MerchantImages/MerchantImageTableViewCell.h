//
//  MerchantImageTableViewCell.h
//  Overhaul
//
//  Created by Apple on 8/17/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MerchantImageTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *merchantImageView;
@property (strong, nonatomic) IBOutlet UILabel *merchantNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *certifiedImageView;
@property (strong, nonatomic) IBOutlet UILabel *kmLabel;
@property (strong, nonatomic) IBOutlet UILabel *openNowLabel;
@property (strong, nonatomic) IBOutlet UIButton *reviewButton;
@property (strong, nonatomic) IBOutlet UILabel *numberReviewLabel;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;

@end
