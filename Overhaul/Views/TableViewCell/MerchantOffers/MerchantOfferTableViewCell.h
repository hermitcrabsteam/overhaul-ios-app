//
//  MerchantOfferTableViewCell.h
//  Overhaul
//
//  Created by Apple on 8/17/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MerchantOfferTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *discountLabel;
@property (strong, nonatomic) IBOutlet UIButton *termsAndConditionsButton;

@end
