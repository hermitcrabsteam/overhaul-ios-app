//
//  SettingsNotificationTableViewCell.h
//  Overhaul
//
//  Created by Rajath Kumar on 10/26/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsNotificationTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *toggleButton;

@end
