//
//  SettingsNotificationTableViewCell.m
//  Overhaul
//
//  Created by Rajath Kumar on 10/26/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "SettingsNotificationTableViewCell.h"

@implementation SettingsNotificationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
