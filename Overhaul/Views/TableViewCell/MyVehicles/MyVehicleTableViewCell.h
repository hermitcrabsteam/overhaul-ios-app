//
//  MyVehicleTableViewCell.h
//  Overhaul
//
//  Created by Apple on 8/22/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyVehicleTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *primaryVehicleLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondaryVehicleLabel;

@end
