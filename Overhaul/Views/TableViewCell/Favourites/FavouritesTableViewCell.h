//
//  FavouritesTableViewCell.h
//  Overhaul
//
//  Created by Rajath Kumar on 9/29/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouritesTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (strong, nonatomic) IBOutlet UIImageView *certifiedImageView;
@property (strong, nonatomic) IBOutlet UIButton *ratingButton;
@property (strong, nonatomic) IBOutlet UILabel *numberReviewsLabel;
@property (strong, nonatomic) IBOutlet UIButton *bookmarkedButton;

@end
