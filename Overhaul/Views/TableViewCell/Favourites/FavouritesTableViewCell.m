//
//  FavouritesTableViewCell.m
//  Overhaul
//
//  Created by Rajath Kumar on 9/29/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "FavouritesTableViewCell.h"

@implementation FavouritesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
