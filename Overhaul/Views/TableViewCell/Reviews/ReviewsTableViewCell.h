//
//  ReviewsTableViewCell.h
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *reviewsImageView;
@property (strong, nonatomic) IBOutlet UILabel *reviewsShopNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *reviewsByLabel;


@end
