//
//  ServiceListTableViewCell.h
//  Overhaul
//
//  Created by admin on 14/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UILabel *KMLabel;
@property (strong, nonatomic) IBOutlet UIButton *ratingButton;
@property (strong, nonatomic) IBOutlet UILabel *numberReviewLabel;
@property (strong, nonatomic) IBOutlet UIImageView *certifiedImageView;
@property (strong, nonatomic) IBOutlet UIImageView *bookmarkImageView;
@property (strong, nonatomic) IBOutlet UIButton *bookmarkButton;

@end
