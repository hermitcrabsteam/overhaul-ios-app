//
//  MyReviewsTableViewCell.h
//  Overhaul
//
//  Created by Apple on 8/22/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyReviewsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *myReviewsButton;
@property (strong, nonatomic) IBOutlet UIButton *friendReviews;
@property (strong, nonatomic) IBOutlet UILabel *shopNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *shopImageView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;

@end
