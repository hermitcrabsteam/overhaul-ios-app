//
//  MerchantAddressTableViewCell.h
//  Overhaul
//
//  Created by Apple on 8/17/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MerchantAddressTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UILabel *weekdayLabel;
@property (strong, nonatomic) IBOutlet UILabel *timingLabel;

@end
