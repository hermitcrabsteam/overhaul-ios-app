//
//  HomeCollectionViewCell.h
//  Overhaul
//
//  Created by admin on 21/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (strong, nonatomic) IBOutlet UIImageView *categoryBackgrdImageView;
@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;

@end
