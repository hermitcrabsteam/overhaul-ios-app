
//
//  CustomTextfield.m
//  Fabric
//
//  Created by Jobin on 16/02/16.
//  Copyright © 2016 Adaptiz. All rights reserved.
//

#import "CustomTextfield.h"

@implementation CustomTextfield

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}


- (CGRect)textRectForBounds:(CGRect)bounds
{
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.frame.size.height - 1, self.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5].CGColor;
    [self.layer addSublayer:bottomBorder];
    
    
    return CGRectMake(bounds.origin.x, bounds.origin.y,
                      bounds.size.width, bounds.size.height);
}


- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
}

@end
