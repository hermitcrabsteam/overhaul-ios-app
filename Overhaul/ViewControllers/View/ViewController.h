//
//  ViewController.h
//  Overhaul
//
//  Created by Adaptiz on 22/06/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "WebCommunicationHandler.h"
#import "Constant.h"
#import "SignUpViewController.h"


@interface ViewController : UIViewController <GIDSignInUIDelegate, WebCommunicationHandlerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)onClickLoginButton:(id)sender;
- (IBAction)onClickForgotPasswordButton:(id)sender;
- (IBAction)onClickSignUpButton:(id)sender;
- (IBAction)onClickFacebookButton:(id)sender;
- (IBAction)onClickGooglePlusButton:(id)sender;

@end

