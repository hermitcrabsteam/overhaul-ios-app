//
//  ViewController.m
//  Overhaul
//
//  Created by Adaptiz on 22/06/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "ViewController.h"
#import "AddVehicleViewController.h"
#import "ForgotPwdViewController.h"

@interface ViewController ()
{
    AppDelegate *appDelegate;
    DHValidation *validation;
    NSMutableCharacterSet *characterSet1;
}

@end

@implementation ViewController 

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    appDelegate = kAppDelegate;
    
    validation=[[DHValidation alloc]init];
    characterSet1 =[[NSMutableCharacterSet alloc]init];
    [characterSet1 addCharactersInString:@"0123456789"];
    
    // TODO(developer) Configure the sign-in button look/feel
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    // Uncomment to automatically sign in the user.
    //[[GIDSignIn sharedInstance] signInSilently];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GooglePlusData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(googleRegistration) name:@"GooglePlusData" object:nil];
    
    
    UIColor *color = [UIColor colorWithRed:83.0/255.0 green:83.0/255.0 blue:83.0/255.0 alpha:1];
    self.mobileNumberTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:@{NSForegroundColorAttributeName: color}];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    
    UITouch *touch = [touches anyObject];
    CGPoint touch_point = [touch locationInView:self.view];
    
    if ([self.view pointInside:touch_point withEvent:event])
    {
        [self resignKeyboard];
    }
}


// Implement these methods only if the GIDSignInUIDelegate is not a subclass of
// UIViewController.

// Stop the UIActivityIndicatorView animation that was started when the user
// pressed the Sign In button

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}




-(void)googleRegistration
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    HomeViewController *homeInstance =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
    [self.navigationController pushViewController:homeInstance animated:YES];
}



- (IBAction)onClickLoginButton:(id)sender
{
    // NSLog(@"%lu", self.mobileNumberTextField.text.length);
    
    if (![validation validateNotEmpty:self.mobileNumberTextField.text])
    {
        [self showAlertWithMessage:@"Please enter the Mobile Number"];
        self.mobileNumberTextField.placeholder=@"Mobile Number";
        self.mobileNumberTextField.text=@"";
    }
    else if (![validation validateStringInCharacterSet:self.mobileNumberTextField.text characterSet:characterSet1])
    {
        [self showAlertWithMessage:@"Please enter valid Mobile Number"];
        self.mobileNumberTextField.placeholder=@"Mobile Number";
        self.mobileNumberTextField.text=@"";
    }
    else if (self.mobileNumberTextField.text.length>10 || self.mobileNumberTextField.text.length<10)
    {
        [self showAlertWithMessage:@"Please enter valid Mobile Number"];
        self.mobileNumberTextField.placeholder=@"Mobile Number";
        self.mobileNumberTextField.text=@"";
    }
    else if (![validation validateNotEmpty:self.passwordTextField.text])
    {
        [self showAlertWithMessage:@"Please enter Password"];
        self.passwordTextField.placeholder=@"Password";
        self.passwordTextField.text=@"";
    }
    else
    {
        [self callingServiceForLogin];
    }
}


-(void)showAlertWithMessage:(NSString *)strmsg
{
    [appDelegate showAlertViewWithMessage:strmsg WithTitle:@"ISO Team" cancelButton:@"OK"];
}


#pragma UITextField Delegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
    
    if (textField.tag==1) {
        
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIToolbar * keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    keyboardToolBar.barStyle = UIBarStyleDefault;
    keyboardToolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)]];
    textField.inputAccessoryView = keyboardToolBar;
}




- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}



-(IBAction)resignKeyboard
{
    [self.mobileNumberTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}



- (IBAction)onClickForgotPasswordButton:(id)sender {
    
     [self performSegueWithIdentifier:@"segueForgotPwdView" sender:self];

}

- (IBAction)onClickSignUpButton:(id)sender {
    
    [self performSegueWithIdentifier:@"segueSignUpView" sender:self];
}


- (IBAction)onClickFacebookButton:(id)sender {
    
    if ([self isNetworkAvailable])
    {
        appDelegate.isLoginFrom = @"Facebook";
        
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logOut];
        
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        
        [login logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         {
             if (error)
             {
                 NSLog(@"Login process error");
             }
             else if (result.isCancelled)
             {
                 NSLog(@"User cancelled login");
             }
             else
             {
                 NSLog(@"Login Success");
                 
                 if ([result.grantedPermissions containsObject:@"email"])
                 {
                     NSLog(@"result is:%@",result);
                     [self fetchUserInfo];
                 }
                 else
                 {
                     //  [SVProgressHUD showErrorWithStatus:@"Facebook email permission error"];
                 }
             }
         }];
    }
}


- (IBAction)onClickGooglePlusButton:(id)sender {
    
    if ([self isNetworkAvailable])
    {
        appDelegate.isLoginFrom = @"GooglePlus";
        
        [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;
        [self GooglePlusLogout];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[GIDSignIn sharedInstance] signIn];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}


-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[FBSDKAccessToken currentAccessToken].tokenString);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email,picture"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"results:%@",result);
                 
                 appDelegate.socialLoginDetails = result;
                 NSString *email = result[@"email"];
                 
                 if (email.length >0 )
                 {
                     HomeViewController *homeInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
                     [self.navigationController pushViewController:homeInstance animated:YES];
                 }
                 else
                 {
                     NSLog(@"Facebook email is not verified");
                 }
             }
             else
             {
                 NSLog(@"Error %@",error);
                 
                 if (error)
                 {
                     if(error.code == 5)
                     {
                         return;
                     }
                     
                     NSString* message = error.localizedDescription;
                     
                     if(error.code == 2)
                     {
                         message = @"To enable Facebook login, open Settings, select the Facebook menu, and set Swachh map to ON.";
                     }
                     
                     NSString *errorTitleString = @"ERROR";
                     
                     [appDelegate showAlertViewWithMessage:[NSString stringWithFormat:@"%@ %@", errorTitleString,message] WithTitle:@"ISOTeam" cancelButton:@"OK"];
                 }
                 
             }
         }];
    }
}


-(void)GooglePlusLogout
{
    [[GIDSignIn sharedInstance] signOut];
}


- (BOOL)isNetworkAvailable
{
    Reachability *internetReachabilityTest = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [internetReachabilityTest currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please check your internet connection" WithTitle:@"ISOTeam" cancelButton:@"OK"];
        
        // [SVProgressHUD dismiss];
        
        return NO;
    }
    else
    {
        NSLog (@"Connection is Available");
        return YES;
    }
}



#pragma WebCommunication Delegate Methods

-(void)callingServiceForLogin
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl = [NSString stringWithFormat:@"%@%@",BaseURL,UrlLogin];
    
    NSDictionary *params=@{
                           @"phone":self.mobileNumberTextField.text,
                           @"password":self.passwordTextField.text,
                           };
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"Login"];
}


#pragma mark - TTLCommunicationHandlerDelegate methods

- (void)communicationHandler  : (WebCommunicationHandler *)handler
downloadDidCompleteWithResponse: (NSHTTPURLResponse *)serverResponse
                        error : (NSError *) error
              responseObject  : (id) responseObject
                      context : (NSString *) context;
{
    NSLog(@"responseObject:%@",responseObject);
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (responseObject && error == nil)
    {
        if ([@"Login"isEqualToString:context])
        {
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                if ([[responseObject valueForKey:@"Status"]isEqualToString:@"Success"])
                {
                    appDelegate.userID = responseObject[@"User"][@"userId"];
                    
                    AddVehicleViewController *addVehicleInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"AddVehicleView"];
                    [self.navigationController pushViewController:addVehicleInstance animated:YES];
                }
                else
                {
//                    AddVehicleViewController *addVehicleInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"AddVehicleView"];
//                    [self.navigationController pushViewController:addVehicleInstance animated:YES];
                    
                    [appDelegate showAlertViewWithMessage:[responseObject valueForKey:@"User"] WithTitle:@"Overhaul" cancelButton:@"OK"];
                }
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
    }
    else
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"segueSignUpView"]) {
        SignUpViewController *signUpInstance = (SignUpViewController *)segue.destinationViewController;
        signUpInstance.isComingFrom = @"LoginScreen";
    } else if ([[segue identifier] isEqualToString:@"segueForgotPwdView"]) {
        ForgotPwdViewController *forgotInstance = (ForgotPwdViewController *)segue.destinationViewController;;
        forgotInstance.forgotPwdString = self.mobileNumberTextField.text;
    }
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
