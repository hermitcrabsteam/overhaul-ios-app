//
//  MerchantViewController.m
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "MerchantViewController.h"
#import "AppDelegate.h"

@interface MerchantViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation MerchantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = kAppDelegate;
}




#pragma UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    
    switch (section)
    {
        case 1:
            sectionName = NSLocalizedString(@"Address", @"Address");
            break;
        case 2:
            sectionName = NSLocalizedString(@"Offers", @"Offers");
            break;
        case 3:
            sectionName = NSLocalizedString(@"Services", @"Services");
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        
        return 0;
    }
    
    
    return 30.0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case 0:
            return 250;
            break;
        case 1:
            return 70;
            break;
        case 2:
            return 60;
            break;
        case 3:
            return 200;
            break;
            
        default:
            return 60;
            break;
    }

    return 0;
}



- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",(int)indexPath.row);
    
    return NO;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        MerchantImageTableViewCell *imageCell = [tableView dequeueReusableCellWithIdentifier:@"MerchantImageTableViewCell"];
        
        [imageCell.backButton addTarget:self action:@selector(onClickBackButton) forControlEvents:UIControlEventTouchUpInside];
        
        [imageCell.shareButton addTarget:self action:@selector(onClickShareButton) forControlEvents:UIControlEventTouchUpInside];
        
        [imageCell.homeButton addTarget:self action:@selector(onClickHomeButton) forControlEvents:UIControlEventTouchUpInside];
        
        return imageCell;
    }
    else if (indexPath.section == 1)
    {
        MerchantAddressTableViewCell *addressCell = [tableView dequeueReusableCellWithIdentifier:@"MerchantAddressTableViewCell"];
        
        addressCell.addressLabel.text = (self.merchantDetailDict)[@"address"];
        
        NSString *worksFrom = (self.merchantDetailDict)[@"worksFrom"];
        NSString *worksUpto = (self.merchantDetailDict)[@"worksUpto"];
        
        addressCell.timingLabel.text = [NSString stringWithFormat:@"%@ - %@",worksFrom,worksUpto];
        
        return addressCell;
    }
    else if (indexPath.section == 2)
    {
        MerchantOfferTableViewCell *offerCell = [tableView dequeueReusableCellWithIdentifier:@"MerchantOfferTableViewCell"];
        
        return offerCell;
    }
    else
    {
        MerchantServicesTableViewCell *serviceListCell = [tableView dequeueReusableCellWithIdentifier:@"MerchantServicesTableViewCell"];
        
        return serviceListCell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    if (section == 0 || section == 1 || section == 2 || section == 3)
//    {
//        return 0;
//    }
//    else
//    {
//        return 45.0;
//    }
//    
//    return 0;
//}
//
//
//- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    MerchantFooterTableViewCell *footerCell = [tableView dequeueReusableCellWithIdentifier:@"MerchantFooterTableViewCell"];
//    
//    
//    return footerCell;
//}


-(void) onClickBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) onClickShareButton
{
    NSString *textToShare = @"Look at this awesome website for Apple Developer Program!";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.apple.com/"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


-(void) onClickHomeButton
{
    HomeViewController *homeInstance =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
    [self.navigationController pushViewController:homeInstance animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
