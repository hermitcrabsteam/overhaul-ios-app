//
//  MerchantViewController.h
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MerchantImageTableViewCell.h"
#import "MerchantAddressTableViewCell.h"
#import "MerchantOfferTableViewCell.h"
#import "MerchantServicesTableViewCell.h"
#import "MerchantFooterTableViewCell.h"

@interface MerchantViewController : UIViewController

@property (strong, nonatomic) NSDictionary *merchantDetailDict;
@property (strong, nonatomic) IBOutlet UITableView *merchantTableView;

@end
