//
//  AboutUsViewController.h
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsViewController : UIViewController

- (IBAction)onClickBackButton:(id)sender;

@end
