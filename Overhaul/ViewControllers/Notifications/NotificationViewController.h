//
//  NotificationViewController.h
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationTableViewCell.h"

@interface NotificationViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *generalButton;
@property (strong, nonatomic) IBOutlet UIButton *offersButton;
@property (strong, nonatomic) IBOutlet UILabel *generalLabel;
@property (strong, nonatomic) IBOutlet UILabel *offersLabel;

- (IBAction)onClickCloseButton:(id)sender;
- (IBAction)onClickGeneralButton:(id)sender;
- (IBAction)onClickOffersButton:(id)sender;

@end
