//
//  FilterViewController.h
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterViewController : UIViewController

- (IBAction)onClickBackButton:(id)sender;
- (IBAction)onClickClearButton:(id)sender;

@end
