//
//  SignUpViewController.m
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "SignUpViewController.h"
#import "OTPViewController.h"

@interface SignUpViewController () <verifyPassword>
{
    AppDelegate *appDelegate;
    DHValidation *validation;
    NSMutableCharacterSet *characterSet1;
}

@end

@implementation SignUpViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = kAppDelegate;
    
    validation=[[DHValidation alloc]init];
    characterSet1 =[[NSMutableCharacterSet alloc]init];
    [characterSet1 addCharactersInString:@"0123456789"];
    
    [self.buisinessTypeTableView setHidden:YES];
    self.buisinessTypeArray = @[@"Service & Maintenance",@"Repairs",@"Fuel Stations",@"Spare Parts",@"Accessories",@"Performance Upgrade",@"Car Wash",@"Tyres"];
    
    // TODO(developer) Configure the sign-in button look/feel
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    // Uncomment to automatically sign in the user.
    //[[GIDSignIn sharedInstance] signInSilently];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GooglePlusData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(googleRegistration) name:@"GooglePlusData" object:nil];
    
    [self.userView setHidden:NO];
    [self.userLineLabel setHidden:NO];
    
    [self.merchantView setHidden:YES];
    [self.merchantLineLabel setHidden:YES];

    
//    [self.verifyMobileNumberButton setEnabled:NO];
//    [self.verifyMobileNumberButton setTitleColor:[UIColor colorWithRed:218.0/255.0 green:104.0/255.0 blue:98.0/255.0 alpha:1] forState:UIControlStateNormal];
    
    self.buisinessTypeTableView.layer.borderWidth = 0.5;
    self.buisinessTypeTableView.layer.borderColor = [UIColor grayColor].CGColor;
    self.buisinessTypeTableView.layer.cornerRadius = 5.0f;
    [self.buisinessTypeTableView setClipsToBounds:YES];
    
    UIColor *color = [UIColor colorWithRed:83.0/255.0 green:83.0/255.0 blue:83.0/255.0 alpha:1];
    self.userNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:@{NSForegroundColorAttributeName: color}];
    self.mobileNumberTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:@{NSForegroundColorAttributeName: color}];
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Id" attributes:@{NSForegroundColorAttributeName: color}];
    
    self.merchantFirstNameTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:@{NSForegroundColorAttributeName: color}];
    self.merchantMobileNumberTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:@{NSForegroundColorAttributeName: color}];
    self.merchantEmailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Id" attributes:@{NSForegroundColorAttributeName: color}];
    self.buisinessTypeTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Buisines Type" attributes:@{NSForegroundColorAttributeName: color}];
    
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    self.confirmPasswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    NSLog(@"Confirm Button : %@", self.confirmButtonDelegateFrom);
    NSLog(@"Close Button : %@", self.closeButtonDelegateFrom);
    
    
    if ([self.isComingFrom isEqualToString:@"LoginScreen"])
    {
        [self.verifyMobileNumberButton setTitle:@"Verify Mobile Number" forState:UIControlStateNormal];
        
        [self.socialView setHidden:NO];
        [self.passwordView setHidden:YES];
    }
    else if ([self.closeButtonDelegateFrom isEqualToString:@"isFromCloseButton"])
    {
        [self.socialView setHidden:NO];
        [self.passwordView setHidden:YES];
    }
    else
    {
        if ([self.eitherOrString isEqualToString:@"UserRegister"]) {
            
            [self.merchantButton setEnabled:NO];
            
            self.userNameTextField.text = (self.params)[@"name"];
            self.emailTextField.text = (self.params)[@"email"];
            self.mobileNumberTextField.text = (self.params)[@"phone"];
        }
        else
        {
            [self.userButton setEnabled:NO];
            
            self.merchantFirstNameTextfield.text = (self.params)[@"name"];
            self.merchantEmailTextField.text = (self.params)[@"email"];
            self.buisinessTypeTextField.text = (self.params)[@"buisinessType"];
            self.merchantMobileNumberTextField.text = (self.params)[@"phone"];
        }
        
        [self.socialView setHidden:YES];
        [self.passwordView setHidden:NO];
    }
}


-(void)googleRegistration
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    HomeViewController *homeInstance =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
    [self.navigationController pushViewController:homeInstance animated:YES];
}



- (IBAction)onClickBackButton:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onClickUserButton:(id)sender {
    
    [self.userView setHidden:NO];
    [self.userLineLabel setHidden:NO];
    
    [self.merchantView setHidden:YES];
    [self.merchantLineLabel setHidden:YES];
    
}

- (IBAction)onClickMerchantButton:(id)sender {
    
    [self.userView setHidden:YES];
    [self.userLineLabel setHidden:YES];
    
    [self.merchantView setHidden:NO];
    [self.merchantLineLabel setHidden:NO];
}


- (IBAction)onClickFacebookButton:(id)sender {
    
    if ([self isNetworkAvailable])
    {
        appDelegate.isLoginFrom = @"Facebook";
        
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logOut];
        
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        
        [login logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         {
             if (error)
             {
                 NSLog(@"Login process error");
             }
             else if (result.isCancelled)
             {
                 NSLog(@"User cancelled login");
             }
             else
             {
                 NSLog(@"Login Success");
                 
                 if ([result.grantedPermissions containsObject:@"email"])
                 {
                     NSLog(@"result is:%@",result);
                     [self fetchUserInfo];
                 }
                 else
                 {
                     //  [SVProgressHUD showErrorWithStatus:@"Facebook email permission error"];
                 }
             }
         }];
    }
}


- (IBAction)onClickGooglePlusButton:(id)sender
{
    if ([self isNetworkAvailable])
    {
        appDelegate.isLoginFrom = @"GooglePlus";
        
        [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;
        [self GooglePlusLogout];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[GIDSignIn sharedInstance] signIn];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}


-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[FBSDKAccessToken currentAccessToken].tokenString);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email,picture"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"results:%@",result);
                 
                 appDelegate.socialLoginDetails = result;
                 NSString *email = result[@"email"];
                 
                 if (email.length >0 )
                 {
                     HomeViewController *homeInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
                     [self.navigationController pushViewController:homeInstance animated:YES];
                 }
                 else
                 {
                     NSLog(@"Facebook email is not verified");
                 }
             }
             else
             {
                 NSLog(@"Error %@",error);
                 
                 if (error)
                 {
                     if(error.code == 5)
                     {
                         return;
                     }
                     
                     NSString* message = error.localizedDescription;
                     
                     if(error.code == 2)
                     {
                         message = @"To enable Facebook login, open Settings, select the Facebook menu, and set Swachh map to ON.";
                     }
                     
                     NSString *errorTitleString = @"ERROR";
                     
                     [appDelegate showAlertViewWithMessage:[NSString stringWithFormat:@"%@ %@", errorTitleString,message] WithTitle:@"ISOTeam" cancelButton:@"OK"];
                 }
             }
         }];
    }
}


-(void)GooglePlusLogout
{
    [[GIDSignIn sharedInstance] signOut];
}


- (BOOL)isNetworkAvailable
{
    Reachability *internetReachabilityTest = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [internetReachabilityTest currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please check your internet connection" WithTitle:@"ISOTeam" cancelButton:@"OK"];
        
        // [SVProgressHUD dismiss];
        
        return NO;
    }
    else
    {
        NSLog (@"Connection is Available");
        return YES;
    }
}




#pragma UITextField Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.buisinessTypeArray).count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 34.0;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"CellId";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = (self.buisinessTypeArray)[indexPath.row];
    cell.textLabel.textAlignment = NSTextAlignmentNatural;
    
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.buisinessTypeTextField.text = (self.buisinessTypeArray)[indexPath.row];

    tableView.hidden = YES;
    
    [self resignKeyboard];
}




#pragma UITextField Delegate Methods

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
    
    if (textField.tag==8)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}



- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIToolbar * keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    keyboardToolBar.barStyle = UIBarStyleDefault;
    keyboardToolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)]];
    textField.inputAccessoryView = keyboardToolBar;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.mobileNumberTextField || textField == self.merchantMobileNumberTextField)
    {
//        NSLog(@"%lu",self.mobileNumberTextField.text.length);
//        
//        int textLength = (int)self.mobileNumberTextField.text.length;
//        int merchantTextLength = (int)self.merchantMobileNumberTextField.text.length;
        
        [self.verifyMobileNumberButton setEnabled:YES];
        [self.verifyMobileNumberButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
//        if (textLength>10 || textLength < 9 || merchantTextLength >10 || merchantTextLength<9)
//        {
//            [self showAlertWithMessage:@"Please enter valid Mobile Number"];
//            self.mobileNumberTextField.placeholder=@"Mobile Number";
//            self.mobileNumberTextField.text=@"";
//        }
//        else
//        {
//          
//        }
    }
}


-(void)resignKeyboard
{
    [self.userNameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.mobileNumberTextField resignFirstResponder];
    
    [self.merchantFirstNameTextfield resignFirstResponder];
    [self.merchantEmailTextField resignFirstResponder];
    [self.merchantMobileNumberTextField resignFirstResponder];
    [self.buisinessTypeTextField resignFirstResponder];
    
    [self.passwordTextField resignFirstResponder];
    [self.confirmPasswordTextField resignFirstResponder];
}


-(void)showAlertWithMessage:(NSString *)strmsg
{
    [appDelegate showAlertViewWithMessage:strmsg WithTitle:@"ISO Team" cancelButton:@"OK"];
}


- (IBAction)onClickVerifyMobileNumber:(id)sender {
    
    self.isComingFrom = @"";
    
    if (self.userLineLabel.hidden == NO) {
        
        if (![validation validateNotEmpty:self.userNameTextField.text])
        {
            [self showAlertWithMessage:@"Please set the Password"];
            self.passwordTextField.placeholder=@"Password";
            self.passwordTextField.text=@"";
        }
//        else if (![validation validateNotEmpty:self.emailTextField.text])
//        {
//            [self showAlertWithMessage:@"Please enter the Email"];
//            self.emailTextField.placeholder=@"Email";
//            self.emailTextField.text=@"";
//        }
//        else if (![validation validateEmail:self.emailTextField.text])
//        {
//            [self showAlertWithMessage:@"Please enter the valid Email"];
//            self.emailTextField.placeholder=@"Email";
//            self.emailTextField.text=@"";
//        }
        else if (![validation validateNotEmpty:self.mobileNumberTextField.text])
        {
            [self showAlertWithMessage:@"Please enter the Mobile Number"];
            self.mobileNumberTextField.placeholder=@"Mobile Number";
            self.mobileNumberTextField.text=@"";
        }
        else if (![validation validateStringInCharacterSet:self.mobileNumberTextField.text characterSet:characterSet1])
        {
            [self showAlertWithMessage:@"Please enter valid Mobile Number"];
            self.mobileNumberTextField.placeholder=@"Mobile Number";
            self.mobileNumberTextField.text=@"";
        }
        else
        {
            [self callingServiceForUserSignUp];
        }
    }
    else
    {
        if (![validation validateNotEmpty:self.merchantFirstNameTextfield.text])
        {
            [self showAlertWithMessage:@"Please set the Password"];
            self.passwordTextField.placeholder=@"Password";
            self.passwordTextField.text=@"";
        }
        else if (![validation validateNotEmpty:self.buisinessTypeTextField.text])
        {
            [self showAlertWithMessage:@"Please set the Business Type"];
            self.buisinessTypeTextField.placeholder=@"Business Type";
            self.buisinessTypeTextField.text=@"";
        }
//        else if (![validation validateNotEmpty:self.emailTextField.text])
//        {
//            [self showAlertWithMessage:@"Please enter the Email"];
//            self.emailTextField.placeholder=@"Email";
//            self.emailTextField.text=@"";
//        }
//        else if (![validation validateEmail:self.emailTextField.text])
//        {
//            [self showAlertWithMessage:@"Please enter the valid Email"];
//            self.emailTextField.placeholder=@"Email";
//            self.emailTextField.text=@"";
//        }
        else if (![validation validateNotEmpty:self.merchantMobileNumberTextField.text])
        {
            [self showAlertWithMessage:@"Please enter the Mobile Number"];
            self.mobileNumberTextField.placeholder=@"Mobile Number";
            self.mobileNumberTextField.text=@"";
        }
        else if (![validation validateStringInCharacterSet:self.merchantMobileNumberTextField.text characterSet:characterSet1])
        {
            [self showAlertWithMessage:@"Please enter valid Mobile Number"];
            self.mobileNumberTextField.placeholder=@"Mobile Number";
            self.mobileNumberTextField.text=@"";
        }
        else
        {
            [self callingServiceForMerchantSignUp];
        }
    }
}



- (IBAction)onClickConfirmPassword:(id)sender {
    
    if (![validation validateNotEmpty:self.passwordTextField.text])
    {
        [self showAlertWithMessage:@"Please set the Password"];
        self.passwordTextField.placeholder=@"Password";
        self.passwordTextField.text=@"";
    }
    else if (![validation validateNotEmpty:self.confirmPasswordTextField.text])
    {
        [self showAlertWithMessage:@"Please set the Confirm Password"];
        self.confirmPasswordTextField.placeholder=@"Confirm Password";
        self.confirmPasswordTextField.text=@"";
    }
    else if (! (self.passwordTextField.text == self.confirmPasswordTextField.text))
    {
        [self showAlertWithMessage:@"Please enter valid Password"];
        self.mobileNumberTextField.placeholder=@"Mobile Number";
        self.mobileNumberTextField.text=@"";
        
        self.confirmPasswordTextField.placeholder=@"Confirm Password";
        self.confirmPasswordTextField.text=@"";
    }
    else
    {
        [self callingServiceForUserSetPassword];
    }
}


- (IBAction)onClickBuisinessTypeDropDownButton:(UIButton *)sender {
    
    if (sender.tag == 1)
    {
        self.buisinessTypeTableView.hidden = NO;
        
        sender.tag = 265;
    }
    else
    {
        self.buisinessTypeTableView.hidden = YES;
        
        sender.tag = 1;
    }
}



-(void)callingServiceForUserSignUp
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlRegisterUser];
    
    self.params=@{
                  @"name":self.userNameTextField.text,
                  @"phone":self.mobileNumberTextField.text,
                  @"sysCityId":@"1",
                  @"imagePath" : @"http://localhost:8088/overhaul/registerUser"
                  };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:self.params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"UserRegister"];
}


-(void)callingServiceForMerchantSignUp
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlRegisterUser];
    
    self.params=@{
                           @"name":self.userNameTextField.text,
                           @"phone":self.mobileNumberTextField.text,
                           @"buisinessType":@"1",
                           @"email" : @"dummy@gmail.com"
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:self.params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"MerchantRegister"];
}


-(void)callingServiceForUserSetPassword
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlSetUserPassword];
    
    NSDictionary *params=@{
                           @"phone":self.mobileNumberTextField.text,
                           @"otp":self.otpNumber,
                           @"password":self.passwordTextField.text,
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"SetUserPwd"];
}


-(void)callingServiceForMerchantSetPassword
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlSetMerchantPassword];
    
    NSDictionary *params=@{
                           @"phone":self.mobileNumberTextField.text,
                           @"otp":self.otpNumber,
                           @"password":self.passwordTextField.text,
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"SetMerchantPwd"];
}


#pragma WebCommunication Delegate Methods

-(void)communicationHandler:(WebCommunicationHandler *)handler
downloadDidCompleteWithResponse:(NSHTTPURLResponse *)serverResponse
                      error:(NSError *)error
             responseObject:(id)responseObject
                    context:(NSString *)context
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSLog(@"responseObject:%@",responseObject);
    
    if (responseObject &&(error== nil))
    {
        if ([context isEqualToString:@"UserRegister"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                OTPViewController *otpView = [self.storyboard instantiateViewControllerWithIdentifier:@"OTPView"];
                otpView.isComingFrom = @"VerifyMobileNumber";
                otpView.isDefinedFrom = context;
                otpView.givenMobileNumber = self.mobileNumberTextField.text;
                otpView.paramsDict = self.params;
                
                otpView.delegate = self;
                
                [self.navigationController pushViewController:otpView animated:YES];
            }
            else
            {
//                OTPViewController *otpView = [self.storyboard instantiateViewControllerWithIdentifier:@"OTPView"];
//                otpView.isComingFrom = @"VerifyMobileNumber";
//                otpView.givenMobileNumber = self.mobileNumberTextField.text;
//                [self.navigationController pushViewController:otpView animated:YES];
                
                [appDelegate showAlertViewWithMessage:[NSString stringWithFormat:@"%@",responseObject[@"Status"]] WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([context isEqualToString:@"MerchantRegister"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                OTPViewController *otpView = [self.storyboard instantiateViewControllerWithIdentifier:@"OTPView"];
                otpView.isComingFrom = @"VerifyMobileNumber";
                otpView.givenMobileNumber = self.merchantMobileNumberTextField.text;
                [self.navigationController pushViewController:otpView animated:YES];
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([context isEqualToString:@"SetUserPwd"] || [context isEqualToString:@"SetMerchantPwd"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            else
            {
                [appDelegate showAlertViewWithMessage:responseObject[@"Status"] WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
    }
    else
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
    }
}



-(void) setConfirmButtonString:(NSString *)isFrom
{
    self.confirmButtonDelegateFrom = isFrom;
}


-(void) setCloseButtonString:(NSString *)isFrom
{
    self.closeButtonDelegateFrom = isFrom;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
