//
//  SignUpViewController.h
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebCommunicationHandler.h"
#import <GoogleSignIn/GoogleSignIn.h>


@interface SignUpViewController : UIViewController <GIDSignInUIDelegate, WebCommunicationHandlerDelegate>

@property (retain, nonatomic) NSString *isComingFrom, *confirmButtonDelegateFrom, *closeButtonDelegateFrom, *otpNumber, *eitherOrString;
@property (retain, nonatomic) NSDictionary *params;
@property (strong, nonatomic) NSArray *buisinessTypeArray;
@property (strong, nonatomic) IBOutlet UILabel *userLineLabel;
@property (strong, nonatomic) IBOutlet UILabel *merchantLineLabel;
@property (strong, nonatomic) IBOutlet UIView *userView;
@property (strong, nonatomic) IBOutlet UITextField *userNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *mobileNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UIView *merchantView;
@property (strong, nonatomic) IBOutlet UITextField *merchantFirstNameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *merchantMobileNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *merchantEmailTextField;
@property (strong, nonatomic) IBOutlet UITextField *buisinessTypeTextField;
@property (strong, nonatomic) IBOutlet UITableView *buisinessTypeTableView;
@property (strong, nonatomic) IBOutlet UIButton *verifyMobileNumberButton;
@property (strong, nonatomic) IBOutlet UIView *socialView;
@property (strong, nonatomic) IBOutlet UIView *passwordView;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (strong, nonatomic) IBOutlet UIButton *userButton;
@property (strong, nonatomic) IBOutlet UIButton *merchantButton;
@property (strong, nonatomic) IBOutlet UIButton *buisinessTypeDropDownButton;

- (IBAction)onClickBackButton:(id)sender;
- (IBAction)onClickUserButton:(id)sender;
- (IBAction)onClickMerchantButton:(id)sender;
- (IBAction)onClickVerifyMobileNumber:(id)sender;
- (IBAction)onClickFacebookButton:(id)sender;
- (IBAction)onClickGooglePlusButton:(id)sender;
- (IBAction)onClickConfirmPassword:(id)sender;
- (IBAction)onClickBuisinessTypeDropDownButton:(id)sender;

@end
