//
//  NewsUpdatesViewController.h
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "NewsUpdatesTableViewCell.h"

@interface NewsUpdatesViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *allLabel;
@property (strong, nonatomic) IBOutlet UILabel *latestLabel;
@property (strong, nonatomic) IBOutlet UILabel *popularLabel;
@property (strong, nonatomic) IBOutlet UITableView *newsTableView;

- (IBAction)onClickBackButton:(id)sender;
- (IBAction)onClickFilterButton:(id)sender;
- (IBAction)onClickSearchButton:(id)sender;
- (IBAction)onClickAllButton:(id)sender;
- (IBAction)onClickLatestButton:(id)sender;
- (IBAction)onClickPopularButton:(id)sender;


@end
