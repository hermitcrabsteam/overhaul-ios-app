//
//  NewsUpdatesViewController.m
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "NewsUpdatesViewController.h"

@interface NewsUpdatesViewController ()

@end

@implementation NewsUpdatesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.allLabel setHidden:NO];
    [self.latestLabel setHidden:YES];
    [self.popularLabel setHidden:YES];
}




#pragma UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 6;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 250;
}



- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",(int)indexPath.row);
    
    return YES;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsUpdatesTableViewCell *newsCell = [tableView dequeueReusableCellWithIdentifier:@"NewsUpdatesTableViewCell"];
    
    newsCell.newsView.layer.borderWidth = 0.3;
    newsCell.newsView.layer.borderColor = [UIColor grayColor].CGColor;
    
//    [newsCell.newsImageView setImage:[UIImage imageNamed:@""]];
//    newsCell.newsNameLabel.text = @"";
//    newsCell.dateLabel.text = @"";
//    newsCell.numberLabel.text = @"";
//    newsCell.postedByLabel.text = @"";
    
    return newsCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


- (IBAction)onClickBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)onClickFilterButton:(id)sender {
    
    FilterViewController *filterInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterView"];
    [self.navigationController pushViewController:filterInstance animated:YES];
}

- (IBAction)onClickSearchButton:(id)sender {
    
    
}

- (IBAction)onClickAllButton:(id)sender {
    
    [self.allLabel setHidden:NO];
    [self.latestLabel setHidden:YES];
    [self.popularLabel setHidden:YES];
}

- (IBAction)onClickLatestButton:(id)sender {
    
    [self.allLabel setHidden:YES];
    [self.latestLabel setHidden:NO];
    [self.popularLabel setHidden:YES];
}

- (IBAction)onClickPopularButton:(id)sender {
    
    [self.allLabel setHidden:YES];
    [self.latestLabel setHidden:YES];
    [self.popularLabel setHidden:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
