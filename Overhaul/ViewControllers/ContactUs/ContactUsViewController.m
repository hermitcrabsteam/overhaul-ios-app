//
//  ContactUsViewController.m
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "ContactUsViewController.h"

@interface ContactUsViewController ()

@end

@implementation ContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.contactView.layer.borderWidth = 0.3;
    self.contactView.layer.borderColor = [UIColor grayColor].CGColor;
}


- (IBAction)onClickCallButton:(id)sender {
    
    NSString *phoneNumber = [@"tel://" stringByAppendingString:self.phoneNumberLabel.text];
    NSURL *phoneUrl = [NSURL URLWithString:phoneNumber];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
    else
    {
        UIAlertView *calAlert = [[UIAlertView alloc]initWithTitle:@"Please check the number" message:@"Call facility is not available for this number !!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        
        [calAlert show];
    }

}

- (IBAction)onClickBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
