//
//  ContactUsViewController.h
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (strong, nonatomic) IBOutlet UIView *contactView;

- (IBAction)onClickCallButton:(id)sender;
- (IBAction)onClickBackButton:(id)sender;

@end
