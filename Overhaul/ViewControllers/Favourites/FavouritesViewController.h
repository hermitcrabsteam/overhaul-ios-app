//
//  FavouritesViewController.h
//  Overhaul
//
//  Created by Rajath Kumar on 9/29/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouritesViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *favouritesArray;
@property (strong, nonatomic) IBOutlet UITableView *favouritesTableView;

- (IBAction)onClickBackButton:(id)sender;

@end
