//
//  FavouritesViewController.m
//  Overhaul
//
//  Created by Rajath Kumar on 9/29/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "FavouritesViewController.h"
#import "FavouritesTableViewCell.h"

@interface FavouritesViewController ()

@end

@implementation FavouritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"Favourites Array : %@", self.favouritesArray);
}




#pragma UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.favouritesArray.count;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}



- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",(int)indexPath.row);
    
    return YES;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FavouritesTableViewCell *serviceListCell = [tableView dequeueReusableCellWithIdentifier:@"FavouritesTableViewCell"];
    
    serviceListCell.nameLabel.text = [NSString stringWithFormat:@"%@",(self.favouritesArray)[indexPath.row][@"serviceStation"][@"stationName"]];
    serviceListCell.addressLabel.text = [NSString stringWithFormat:@"%@",(self.favouritesArray)[indexPath.row][@"serviceStation"][@"address"]];
    
    serviceListCell.distanceLabel.text = [NSString stringWithFormat:@"%@ Km",(self.favouritesArray)[indexPath.row][@"serviceStation"][@"distance"]];
    
    [serviceListCell.ratingButton setTitle:[NSString stringWithFormat:@"%@.0",(self.favouritesArray)[indexPath.row][@"serviceStation"][@"rating"]] forState:UIControlStateNormal];
    serviceListCell.numberReviewsLabel.text = [NSString stringWithFormat:@"%@ Reviews",(self.favouritesArray)[indexPath.row][@"serviceStation"][@"noOfReviews"]];
    
    if ([[NSString stringWithFormat:@"%@",(self.favouritesArray)[indexPath.row][@"serviceStation"][@"isCertified"]] isEqualToString:@"1"])
    {
        (serviceListCell.certifiedImageView).image = [UIImage imageNamed:@"Icon_-_certificate_round_REd"];
    }
    else
    {
        (serviceListCell.certifiedImageView).image = [UIImage imageNamed:@"Icon_-_certificate_round_grey"];
    }
    
    
    [serviceListCell.bookmarkedButton setBackgroundImage:[UIImage imageNamed:@"book_mark_red_icon"] forState:UIControlStateNormal];
    
    return serviceListCell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}


- (IBAction)onClickBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
