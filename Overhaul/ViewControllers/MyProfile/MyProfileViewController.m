//
//  MyProfileViewController.m
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "MyProfileViewController.h"

@interface MyProfileViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = kAppDelegate;
    
    [self callingServiceForGetVehiclesByID];
}



#pragma UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    
    switch (section)
    {
        case 1:
            sectionName = NSLocalizedString(@"Personal Info", @"Personal Info");
            break;
        case 2:
            sectionName = NSLocalizedString(@"My Garage", @"My Garage");
            break;
        case 3:
            sectionName = NSLocalizedString(@"Reviews", @"Reviews");
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        
        return 0;
    }
    
    
    return 30.0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case 0:
            return 200;
            break;
        case 1:
            return 270;
            break;
        case 2:
            return 110;
            break;
        case 3:
            return 193;
            break;
        default:
            return 0;
            break;
    }
    
    return 0;
}



- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",(int)indexPath.row);
    
    return NO;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        MyProfileTableViewCell *imageCell = [tableView dequeueReusableCellWithIdentifier:@"MyProfileTableViewCell"];
        
        [imageCell.menuButton addTarget:self action:@selector(onClickMenuButton) forControlEvents:UIControlEventTouchUpInside];
        
        [imageCell.notificationButton addTarget:self action:@selector(onClickNotificationButton) forControlEvents:UIControlEventTouchUpInside];
        
        [imageCell.editButton addTarget:self action:@selector(onClickEditButton) forControlEvents:UIControlEventTouchUpInside];
        
        return imageCell;
    }
    else if (indexPath.section == 1)
    {
        MyDetailsTableViewCell *detailCell = [tableView dequeueReusableCellWithIdentifier:@"MyDetailsTableViewCell"];
        
        return detailCell;
    }
    else if (indexPath.section == 2)
    {
        MyVehicleTableViewCell *detailCell = [tableView dequeueReusableCellWithIdentifier:@"MyVehicleTableViewCell"];
        
        return detailCell;
    }
    else
    {
        MyReviewsTableViewCell *reviewsCell = [tableView dequeueReusableCellWithIdentifier:@"MyReviewsTableViewCell"];
        
        return reviewsCell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}



-(void) onClickMenuButton
{
    MenuViewController *menuInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    [self.navigationController pushViewController:menuInstance animated:YES];
}


-(void) onClickNotificationButton
{
    NotificationViewController *notificationInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationView"];
    [self.navigationController pushViewController:notificationInstance animated:YES];
}


-(void) onClickEditButton
{

}



-(void) callingServiceForGetVehiclesByID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlGetVehiclesByID];
    
    NSDictionary *params=@{
                           @"userId" : @"1"
                           };
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"GetVehicle"];
}


#pragma WebCommunication Delegate Methods

-(void)communicationHandler:(WebCommunicationHandler *)handler
downloadDidCompleteWithResponse:(NSHTTPURLResponse *)serverResponse
                      error:(NSError *)error
             responseObject:(id)responseObject
                    context:(NSString *)context
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSLog(@"responseObject:%@",responseObject);
    
    if (responseObject &&(error== nil))
    {
        if ([context isEqualToString:@"GetVehicle"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                self.vehicleArray = [NSArray arrayWithArray:responseObject[@"Vehicles"]];
                
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
    }
    else
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
