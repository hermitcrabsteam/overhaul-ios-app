//
//  MyProfileViewController.h
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "MyProfileTableViewCell.h"
#import "MyDetailsTableViewCell.h"
#import "MyVehicleTableViewCell.h"
#import "MyReviewsTableViewCell.h"
#import "MenuViewController.h"
#import "NotificationViewController.h"

@interface MyProfileViewController : UIViewController <WebCommunicationHandlerDelegate>

@property (nonatomic, strong) NSArray *vehicleArray;

@end
