//
//  ServiceListViewController.m
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "ServiceListViewController.h"
#import "AppDelegate.h"
#import "FavouritesViewController.h"

@interface ServiceListViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation ServiceListViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = kAppDelegate;
    
    [self.distanceLabel setHidden:NO];
    [self.ratingLabel setHidden:YES];
    
    self.sendFavArray = [[NSMutableArray alloc] init];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
//    NSUserDefaults *defaults1 = [[NSUserDefaults alloc] init];
//    self.sendFavArray = [defaults1 objectForKey:@"FavArray"];
}

#pragma UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.serviceStationsArray.count;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}



- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",(int)indexPath.row);
    
    return YES;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ServiceListTableViewCell *serviceListCell = [tableView dequeueReusableCellWithIdentifier:@"ServiceListTableViewCell"];
    
    serviceListCell.nameLabel.text = [NSString stringWithFormat:@"%@",(self.serviceStationsArray)[indexPath.row][@"stationName"]];
    serviceListCell.addressLabel.text = [NSString stringWithFormat:@"%@",(self.serviceStationsArray)[indexPath.row][@"address"]];
    
    serviceListCell.KMLabel.text = [NSString stringWithFormat:@"%@ Km",(self.serviceStationsArray)[indexPath.row][@"distance"]];
    
    [serviceListCell.ratingButton setTitle:[NSString stringWithFormat:@"%@.0",(self.serviceStationsArray)[indexPath.row][@"rating"]] forState:UIControlStateNormal];
    serviceListCell.numberReviewLabel.text = [NSString stringWithFormat:@"%@ Reviews",(self.serviceStationsArray)[indexPath.row][@"noOfReviews"]];

    if ([[NSString stringWithFormat:@"%@",(self.serviceStationsArray)[indexPath.row][@"isCertified"]] isEqualToString:@"1"])
    {
        (serviceListCell.certifiedImageView).image = [UIImage imageNamed:@"Icon_-_certificate_round_REd"];
    }
    else
    {
        (serviceListCell.certifiedImageView).image = [UIImage imageNamed:@"Icon_-_certificate_round_grey"];
    }
    
    self.favServiceStationID = [NSString stringWithFormat:@"%@",(self.serviceStationsArray)[indexPath.row][@"serviceStationId"]];
    serviceListCell.bookmarkButton.tag = (int)indexPath.row;
    [serviceListCell.bookmarkButton addTarget:self action:@selector(bookmarksButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [serviceListCell.bookmarkButton setBackgroundImage:[UIImage imageNamed:@"book_mark_grey_icon"] forState:UIControlStateNormal];
    
    return serviceListCell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.serviceStationID = [NSString stringWithFormat:@"%@",(self.serviceStationsArray)[indexPath.row][@"serviceStationId"]];
    
    [self callingServiceStationDetailByID:self.serviceStationID];
}


-(void) bookmarksButtonTapped:(UIButton *) sender
{
    arrayIndex = (int)sender.tag;
    [self callingServiceAddFavourite:self.favServiceStationID];
}


- (IBAction)onClickBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickSearchButton:(id)sender {
    
}


- (IBAction)onClickFilterButton:(id)sender {
    
    FilterViewController *filterInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterView"];
    [self.navigationController pushViewController:filterInstance animated:YES];
}

- (IBAction)onClickDistanceButton:(id)sender {
    
    [self.distanceLabel setHidden:NO];
    [self.ratingLabel setHidden:YES];
}

- (IBAction)onClickRatingButton:(id)sender {
    
    [self.distanceLabel setHidden:YES];
    [self.ratingLabel setHidden:NO];
}


- (IBAction)onClickVehicleDropDownButton:(id)sender {
    
}


- (IBAction)onClickOpenNowButton:(UIButton *)sender {
    
    if (sender.tag == 1)
    {
        [self.openNowToggleButton setBackgroundImage:[UIImage imageNamed:@"toggle disabled"] forState:UIControlStateNormal];
        
        sender.tag = 1066;
    }
    else
    {
        [self.openNowToggleButton setBackgroundImage:[UIImage imageNamed:@"toggle"] forState:UIControlStateNormal];
        
        sender.tag = 1;
    }

}


-(void)callingServiceStationDetailByID:(NSString *)serviceStationID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlServiceStationById];
    
    NSDictionary *params=@{
                           @"serviceStationId" : serviceStationID
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"ServiceStationDetails"];
}


-(void)callingServiceAddFavourite:(NSString *)serviceStationID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlAddFavourite];
    
    NSDictionary *params=@{
                           @"userId" : @"1",  //appDelegate.userID
                           @"serviceStationId" : serviceStationID
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"AddFavourite"];
}


#pragma WebCommunication Delegate Methods

-(void)communicationHandler:(WebCommunicationHandler *)handler
downloadDidCompleteWithResponse:(NSHTTPURLResponse *)serverResponse
                      error:(NSError *)error
             responseObject:(id)responseObject
                    context:(NSString *)context
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSLog(@"responseObject:%@",responseObject);
    
    if (responseObject &&(error== nil))
    {
        if ([context isEqualToString:@"ServiceStationDetails"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                MerchantViewController *merchantInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"MerchantView"];
                merchantInstance.merchantDetailDict = responseObject[@"ServiceStation"];
                [self.navigationController pushViewController:merchantInstance animated:YES];
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([context isEqualToString:@"AddFavourite"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
                ServiceListTableViewCell *cell=(ServiceListTableViewCell *)[self.serviceListTableView cellForRowAtIndexPath:path];
                [cell.bookmarkButton setBackgroundImage:[UIImage imageNamed:@"book_mark_red_icon"] forState:UIControlStateNormal];
                
                [self.sendFavArray addObject:(self.serviceStationsArray)[arrayIndex]];
                
//                NSUserDefaults *defaults2 = [[NSUserDefaults alloc] init];
//                [defaults2 setObject:self.sendFavArray forKey:@"FavArray"];
//                [defaults2 synchronize];
                
                NSLog(@"Sent Favourite Array : %@", self.sendFavArray);
                
                FavouritesViewController *favoInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"FavouriteView"];
                favoInstance.favouritesArray = self.sendFavArray;
                [self.navigationController pushViewController:favoInstance animated:YES];

            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
    }
    else
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
    }
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
