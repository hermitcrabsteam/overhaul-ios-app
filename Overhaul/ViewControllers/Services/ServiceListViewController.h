//
//  ServiceListViewController.h
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebCommunicationHandler.h"
#import "ServiceListTableViewCell.h"

@interface ServiceListViewController : UIViewController <WebCommunicationHandlerDelegate>
{
    int arrayIndex;
}

@property (strong, nonatomic) NSString *serviceStationID, *favServiceStationID;
@property (strong, nonatomic) NSArray  *serviceStationsArray;
@property (strong, nonatomic) NSMutableArray *sendFavArray;
@property (strong, nonatomic) IBOutlet UIButton *distanceButton;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (strong, nonatomic) IBOutlet UIButton *ratingButton;
@property (strong, nonatomic) IBOutlet UILabel *ratingLabel;
@property (strong, nonatomic) IBOutlet UITableView *serviceListTableView;
@property (strong, nonatomic) IBOutlet UIButton *openNowToggleButton;
@property (strong, nonatomic) IBOutlet UIButton *vehicleNameButton;

- (IBAction)onClickBackButton:(id)sender;
- (IBAction)onClickSearchButton:(id)sender;
- (IBAction)onClickFilterButton:(id)sender;
- (IBAction)onClickDistanceButton:(id)sender;
- (IBAction)onClickRatingButton:(id)sender;
- (IBAction)onClickVehicleDropDownButton:(id)sender;
- (IBAction)onClickOpenNowButton:(id)sender;

@end
