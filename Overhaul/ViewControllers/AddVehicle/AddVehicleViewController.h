//
//  AddVehicleViewController.h
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface AddVehicleViewController : UIViewController <WebCommunicationHandlerDelegate>

@property (strong, nonatomic) NSArray *filterdArray;
@property (strong, nonatomic) NSArray *responseArray, *carBrandsArray, *bikeBrandsArray, *dummyCarBrandsArray, *dummyBikeBrandsArray;
@property NSMutableArray *carModelsArray, *bikeModelsArray;
@property (strong, nonatomic) IBOutlet UILabel *fourWheelerLabel;
@property (strong, nonatomic) IBOutlet UILabel *twoWheelerLabel;
@property (strong, nonatomic) IBOutlet UITextField *selectMakerTextField;
@property (strong, nonatomic) IBOutlet UITextField *selectModelTextField;
@property (strong, nonatomic) IBOutlet UIButton *selectMakerDropDownButton;
@property (strong, nonatomic) IBOutlet UIButton *selectModelDropDownButton;
@property (strong, nonatomic) IBOutlet UIButton *fourWheelerButton;
@property (strong, nonatomic) IBOutlet UIButton *twoWheelerButton;
@property (strong, nonatomic) IBOutlet UITableView *makerTableView;
@property (strong, nonatomic) IBOutlet UITableView *modelTableView;

- (IBAction)onClickSkipButton:(id)sender;
- (IBAction)onClickFourWheelerButton:(id)sender;
- (IBAction)onClickTwoWheelerButton:(id)sender;
- (IBAction)onClickMakerDropDownButton:(id)sender;
- (IBAction)onClickModelDropDownButton:(id)sender;
- (IBAction)onClickContinueButton:(id)sender;

@end
