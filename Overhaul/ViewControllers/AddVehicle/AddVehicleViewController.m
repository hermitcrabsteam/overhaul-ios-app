//
//  AddVehicleViewController.m
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "AddVehicleViewController.h"

@interface AddVehicleViewController ()
{
    AppDelegate *appDelegate;
    DHValidation *validation;
    NSMutableCharacterSet *characterSet1;
}

@end

@implementation AddVehicleViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = kAppDelegate;
    
    validation=[[DHValidation alloc]init];
    characterSet1 =[[NSMutableCharacterSet alloc]init];
    [characterSet1 addCharactersInString:@"0123456789"];
    
    [self.fourWheelerLabel setHidden:NO];
    [self.twoWheelerLabel setHidden:YES];
    
    [self.makerTableView setHidden:YES];
    [self.modelTableView setHidden:YES];
    
    self.carModelsArray = [[NSMutableArray alloc] init];
    self.bikeModelsArray = [[NSMutableArray alloc] init];
    
    self.makerTableView.layer.borderWidth = 0.5;
    self.makerTableView.layer.borderColor = [UIColor grayColor].CGColor;
    self.makerTableView.layer.cornerRadius = 5.0f;
    [self.makerTableView setClipsToBounds:YES];
    
    self.modelTableView.layer.borderWidth = 0.5;
    self.modelTableView.layer.borderColor = [UIColor grayColor].CGColor;
    self.modelTableView.layer.cornerRadius = 5.0f;
    [self.modelTableView setClipsToBounds:YES];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self callingServiceForMasterData];
}


- (IBAction)onClickSkipButton:(id)sender {
    
    HomeViewController *homeInstance =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
    [self.navigationController pushViewController:homeInstance animated:YES];
}


- (IBAction)onClickFourWheelerButton:(id)sender {
    
    [self.fourWheelerLabel setHidden:NO];
    [self.twoWheelerLabel setHidden:YES];
}

- (IBAction)onClickTwoWheelerButton:(id)sender {
    
    [self.fourWheelerLabel setHidden:YES];
    [self.twoWheelerLabel setHidden:NO];
}


- (IBAction)onClickMakerDropDownButton:(UIButton *)sender {
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc]init];
    appDelegate.responseDict = [defaults objectForKey:@"VehiclesResponse"];
    
    if (sender.tag == 1)
    {
        if (self.fourWheelerLabel.hidden == NO) {
            
           // self.carBrandsArray = [[appDelegate.responseDict objectForKey:@"CarBrands"] valueForKey:@"sysCarBrandName"];
            
            self.dummyCarBrandsArray = self.carBrandsArray;
        }
        else
        {
           // self.bikeBrandsArray = [[appDelegate.responseDict objectForKey:@"BikeBrands"] valueForKey:@"sysBikeBrandName"];
            
            self.dummyBikeBrandsArray = self.bikeBrandsArray;
        }
        
        sender.tag = 56;
        
        self.makerTableView.hidden = NO;
        self.modelTableView.hidden = YES;
        
        [self.makerTableView reloadData];
    }
    else
    {
        self.makerTableView.hidden = YES;
        self.modelTableView.hidden = YES;
        
        sender.tag = 1;
    }
    
    self.selectModelDropDownButton.tag = 2;
}


- (IBAction)onClickModelDropDownButton:(UIButton *)sender {
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc]init];
    appDelegate.carsResponseArray = [defaults objectForKey:@"CarResponseArray"];
    
    NSString *brandString = self.selectMakerTextField.text;
    
    if (sender.tag == 2)
    {
        if (self.fourWheelerLabel.hidden == NO) {
            
            self.filterdArray = (NSMutableArray *)[appDelegate.carsResponseArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sysCarBrandName beginswith[cd] %@",brandString]];
            
            //NSLog(@"---filter array---%@", filterdArray);
            
            self.carModelsArray = (self.filterdArray)[0][@"sysCarModelName"];
            
            if (self.carModelsArray.count == 0)
            {
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:@"No Models in this Brand"
                                      message:@"Please select another Brand"
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,
                                      nil];
                [alert show];
            }
            else
            {
                self.makerTableView.hidden = YES;
                self.modelTableView.hidden = NO;
                
                [self.modelTableView reloadData];
            }
        }
        else
        {
            if (self.fourWheelerLabel.hidden == NO) {
                
                self.filterdArray = (NSMutableArray *)[appDelegate.bikesResponseArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sysBikeBrandName beginswith[cd] %@",brandString]];
                
                //NSLog(@"---filter array---%@", filterdArray);
                
                self.bikeModelsArray = (self.filterdArray)[0][@"sysBikeModelName"];
                
                if (self.carModelsArray.count == 0)
                {
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle:@"No Models in this Brand"
                                          message:@"Please select another Brand"
                                          delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil,
                                          nil];
                    [alert show];
                }
                else
                {
                    self.makerTableView.hidden = YES;
                    self.modelTableView.hidden = NO;
                    
                    [self.modelTableView reloadData];
                }
            }
        }

    }
    else
    {
        self.makerTableView.hidden = YES;
        self.modelTableView.hidden = YES;
        
        sender.tag = 2;
    }
    
    self.selectMakerDropDownButton.tag = 1;
}


- (IBAction)onClickContinueButton:(id)sender {
    
    if (![validation validateNotEmpty:self.selectMakerTextField.text])
    {
        [self showAlertWithMessage:@"Please select the brand"];
        self.selectMakerTextField.placeholder=@"Select Maker";
        self.selectMakerTextField.text=@"";
    }
    else if (![validation validateNotEmpty:self.selectModelTextField.text])
    {
        [self showAlertWithMessage:@"Please select the model"];
        self.selectModelTextField.placeholder=@"Select Model";
        self.selectModelTextField.text=@"";
    }
    else
    {
        [self callingServiceForAddVehicle];
    }
}


-(void)showAlertWithMessage:(NSString *)strmsg
{
    [appDelegate showAlertViewWithMessage:strmsg WithTitle:@"ISO Team" cancelButton:@"OK"];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 1)
    {
        if (self.fourWheelerLabel.hidden == NO)
        {
            return (self.dummyCarBrandsArray).count;
        }
        else
        {
            return (self.dummyBikeBrandsArray).count;
        }
    }
    else if (tableView.tag == 2)
    {
        if (self.fourWheelerLabel.hidden == NO)
        {
            return (self.carModelsArray).count;
        }
        else
        {
            return (self.bikeModelsArray).count;
        }
    }
    else
        
        return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 34.0;
}
// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"CellId";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    
    
    if (tableView.tag == 1)
    {
        if (self.fourWheelerLabel.hidden == NO)
        {
            cell.textLabel.text = (self.carBrandsArray)[indexPath.row];

        }
        else
        {
            cell.textLabel.text = (self.bikeBrandsArray)[indexPath.row];
        }
    }
    else
    {
        if (self.fourWheelerLabel.hidden == NO)
        {
            cell.textLabel.text = (self.carModelsArray)[indexPath.row];
        }
        else
        {
            cell.textLabel.text = (self.bikeModelsArray)[indexPath.row];
        }
    }
    
    cell.textLabel.textAlignment = NSTextAlignmentNatural;
    
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1)
    {
        if (self.fourWheelerLabel.hidden == NO)
        {
            NSUserDefaults *defaults = [[NSUserDefaults alloc]init];
            appDelegate.carsResponseArray = [defaults objectForKey:@"CarResponseArray"];
            
            self.selectMakerTextField.text = (self.carBrandsArray)[indexPath.row];
            self.selectMakerDropDownButton.tag = 1;
            
            NSString *brandString = [NSString stringWithFormat:@"%@",self.selectMakerTextField.text];
            
            self.filterdArray = (NSMutableArray *)[appDelegate.carsResponseArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sysCarBrandName beginswith[cd] %@",brandString]];
            
            NSLog(@"Filtered Array : %@", self.filterdArray);

//            NSString *searchStr = @"sysCarModelName";
//            NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", searchStr];
//            NSMutableArray *dummyArray = (NSMutableArray *)[[self.filterdArray objectAtIndex:0] objectForKey:@"carModels"];
//            
//            self.carModelsArray = [dummyArray filteredArrayUsingPredicate:pred];;
            
           NSMutableArray *dummyArray = (self.filterdArray)[0][@"carModels"];
            
            for (int i=0 ; i<dummyArray.count ; i++) {
                
                NSString * modelname = dummyArray[i][@"sysCarModelName"];
                
                NSLog(@"Model : %@", modelname);
                
                [self.carModelsArray addObject:modelname];
            }
            
            
            NSLog(@"---models array---%@", self.carModelsArray);
            
            if (self.carModelsArray.count == 0)
            {
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:@"No Models in this Brand"
                                      message:@"Please select another Brand"
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,
                                      nil];
                [alert show];
            }
            else
            {
                self.makerTableView.hidden = YES;
                self.modelTableView.hidden = NO;
                
                [self.modelTableView reloadData];
                
                self.selectMakerDropDownButton.tag = 63;
            }
            
        }
        else
        {
            NSUserDefaults *defaults = [[NSUserDefaults alloc]init];
            appDelegate.carsResponseArray = [defaults objectForKey:@"BikeResponseArray"];
            
            self.selectMakerTextField.text = (self.bikeBrandsArray)[indexPath.row];
            self.selectModelDropDownButton.tag = 2;

            
            NSString *brandString = [NSString stringWithFormat:@"%@",self.selectMakerTextField.text];
            
            self.filterdArray = (NSMutableArray *)[appDelegate.bikesResponseArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sysBikeBrandName beginswith[cd] %@",brandString]];
            
            
            //NSLog(@"---filter array---%@", filterdArray);
            
 
            
            NSMutableArray *dummyArray = (self.filterdArray)[0][@"bikeModels"];
            
            for (int i=0 ; i<dummyArray.count ; i++) {
                
                NSString * modelname = dummyArray[i][@"sysBikeModelName"];
                
                NSLog(@"Model : %@", modelname);
                
                [self.bikeModelsArray addObject:modelname];
            }
            
            
            NSLog(@"---models array---%@", self.carModelsArray);
            
            
            
            if (self.bikeModelsArray.count == 0)
            {
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:@"No Models in this Brand"
                                      message:@"Please select another Brand"
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,
                                      nil];
                [alert show];
            }
            else
            {
                self.makerTableView.hidden = YES;
                self.modelTableView.hidden = NO;
                
                [self.modelTableView reloadData];
                
                self.selectMakerDropDownButton.tag = 63;
            }
        }
    }
    else
    {
        self.modelTableView.hidden = YES;
        
        if (self.fourWheelerLabel.hidden == NO)
        {
            self.selectModelTextField.text = (self.carModelsArray)[indexPath.row];
        }
        else
        {
            self.selectModelTextField.text = (self.bikeModelsArray)[indexPath.row];
        }
    }
    
    self.makerTableView.hidden = YES;
    [self resignKeyboard];
}



#pragma WebCommunication Delegate Methods

-(void)callingServiceForMasterData
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl = [NSString stringWithFormat:@"%@%@",BaseURL,UrlGetAllVehicles];
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:nil
                        WithRequestMethod:@"GET"
                                authorize:NO
                           networkHandler:self
                                  context:@"MasterData"];
}


-(void)callingServiceForAddVehicle
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl = [NSString stringWithFormat:@"%@%@",BaseURL,UrlAddVehicle];
    
    NSDictionary *params=@{
                           @"userId" : @"1",
                           @"sysVehicleTypeId" : @"4",
                           @"sysCarBrandId" : @"4",
                           @"sysCarModelId" : @"2"
                           };
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"AddVehicle"];
}


#pragma mark - TTLCommunicationHandlerDelegate methods

- (void)communicationHandler  : (WebCommunicationHandler *)handler
downloadDidCompleteWithResponse: (NSHTTPURLResponse *)serverResponse
                        error : (NSError *) error
              responseObject  : (id) responseObject
                      context : (NSString *) context;
{
    NSLog(@"responseObject:%@",responseObject);
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (responseObject && error == nil)
    {
        if ([@"MasterData"isEqualToString:context])
        {
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                if ([[responseObject valueForKey:@"Status"]isEqualToString:@"Success"])
                {
                    [appDelegate.responseDict addObject:responseObject];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:appDelegate.responseDict forKey:@"VehiclesResponse"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                     appDelegate.bikesResponseArray = responseObject[@"BikeBrands"];
                     self.bikeBrandsArray = [responseObject[@"BikeBrands"] valueForKey:@"sysBikeBrandName"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:appDelegate.bikesResponseArray forKey:@"BikeResponseArray"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    appDelegate.carsResponseArray = [NSMutableArray arrayWithArray:responseObject[@"CarBrands"]];
                    self.carBrandsArray = [responseObject[@"CarBrands"] valueForKey:@"sysCarBrandName"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:appDelegate.carsResponseArray forKey:@"CarResponseArray"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                else
                {
                    [appDelegate showAlertViewWithMessage:[NSString stringWithFormat:@"%@",responseObject[@"Status"]] WithTitle:@"Overhaul" cancelButton:@"OK"];
                }
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([@"AddVehicle"isEqualToString:context])
        {
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                if ([[responseObject valueForKey:@"Status"]isEqualToString:@"Success"])
                {
                    HomeViewController *homeInstance =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
                    [self.navigationController pushViewController:homeInstance animated:YES];
                    
                    [appDelegate showAlertViewWithMessage:@"Vehicle added successfully" WithTitle:@"Overhaul" cancelButton:@"OK"];
                }
                else
                {
                    [appDelegate showAlertViewWithMessage:[NSString stringWithFormat:@"%@",responseObject[@"Status"]] WithTitle:@"Overhaul" cancelButton:@"OK"];
                }
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
    }
    else
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
    }
}



#pragma UITextField Delegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
    
    if (textField.tag==1) {
        
        [textField resignFirstResponder];
    }
    
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIToolbar * keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    keyboardToolBar.barStyle = UIBarStyleDefault;
    keyboardToolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)]];
    textField.inputAccessoryView = keyboardToolBar;
}




- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}



-(IBAction)resignKeyboard
{
    [self.selectMakerTextField resignFirstResponder];
    [self.selectModelTextField resignFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
