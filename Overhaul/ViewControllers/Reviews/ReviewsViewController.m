//
//  ReviewsViewController.m
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "ReviewsViewController.h"
#import "ReviewsTableViewCell.h"

@interface ReviewsViewController ()

@end

@implementation ReviewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.carsLabel setHidden:NO];
    [self.bikesLabel setHidden:YES];
}



#pragma UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 6;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}



- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",(int)indexPath.row);
    
    return YES;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReviewsTableViewCell *reviewsCell = [tableView dequeueReusableCellWithIdentifier:@"ReviewsTableViewCell"];
    
//    [reviewsCell.reviewsImageView setImage:[UIImage imageNamed:@""]];
//    reviewsCell.reviewsShopNameLabel.text = @"";
//    reviewsCell.reviewsByLabel.text = @"";
    
    return reviewsCell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


- (IBAction)onClickBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickSearchButton:(id)sender {
    
    
}

- (IBAction)onClickFilterButton:(id)sender {
    
    FilterViewController *filterInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterView"];
    [self.navigationController pushViewController:filterInstance animated:YES];
}

- (IBAction)onClickCarsButton:(id)sender {
    
    [self.carsLabel setHidden:NO];
    [self.bikesLabel setHidden:YES];
}

- (IBAction)onClickBikesButton:(id)sender {
    
    [self.carsLabel setHidden:YES];
    [self.bikesLabel setHidden:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
