//
//  ReviewsViewController.h
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface ReviewsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *carsLabel;
@property (strong, nonatomic) IBOutlet UILabel *bikesLabel;
@property (strong, nonatomic) IBOutlet UITableView *reviewsTableView;

- (IBAction)onClickBackButton:(id)sender;
- (IBAction)onClickSearchButton:(id)sender;
- (IBAction)onClickFilterButton:(id)sender;
- (IBAction)onClickCarsButton:(id)sender;
- (IBAction)onClickBikesButton:(id)sender;

@end
