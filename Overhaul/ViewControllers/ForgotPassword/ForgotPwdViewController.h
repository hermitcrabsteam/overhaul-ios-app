//
//  ForgotPwdViewController.h
//  Overhaul
//
//  Created by Rajath Kumar on 10/19/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface ForgotPwdViewController : UIViewController <WebCommunicationHandlerDelegate>

@property (strong, nonatomic) NSString *forgotPwdString;
@property (strong, nonatomic) IBOutlet UITextField *registeredMobileNumField;
@property (strong, nonatomic) IBOutlet UILabel *passwordTitleLabel;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordField;

- (IBAction)onClickCloseButton:(id)sender;
- (IBAction)onClickConfirmButton:(id)sender;

@end
