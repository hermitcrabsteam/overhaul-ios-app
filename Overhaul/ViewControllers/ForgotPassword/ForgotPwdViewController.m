//
//  ForgotPwdViewController.m
//  Overhaul
//
//  Created by Rajath Kumar on 10/19/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "ForgotPwdViewController.h"

@interface ForgotPwdViewController ()
{
    AppDelegate *appDelegate;
    DHValidation *validation;
    NSMutableCharacterSet *characterSet1;
}

@end

@implementation ForgotPwdViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = kAppDelegate;
    
    self.registeredMobileNumField.text = self.forgotPwdString;
    
    validation=[[DHValidation alloc]init];
    characterSet1 =[[NSMutableCharacterSet alloc]init];
    [characterSet1 addCharactersInString:@"0123456789"];
}


- (IBAction)onClickCloseButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)onClickConfirmButton:(id)sender
{
    if (![validation validateNotEmpty:self.registeredMobileNumField.text])
    {
        [self showAlertWithMessage:@"Please enter the registered mobile number"];
        self.registeredMobileNumField.placeholder=@"Registered Mobile Number";
        self.registeredMobileNumField.text=@"";
    }
    else if (![validation validateNotEmpty:self.passwordField.text])
    {
        [self showAlertWithMessage:@"Please set the Password"];
        self.passwordField.placeholder=@"Password";
        self.passwordField.text=@"";
    }
    else if (![validation validateNotEmpty:self.confirmPasswordField.text])
    {
        [self showAlertWithMessage:@"Please set the Confirm Password"];
        self.confirmPasswordField.placeholder=@"Confirm Password";
        self.confirmPasswordField.text=@"";
    }
    else if (![self.passwordField.text isEqualToString:self.confirmPasswordField.text])
    {
        [self showAlertWithMessage:@"Please enter valid Password"];
        self.passwordField.placeholder=@"Enter New Password";
        self.passwordField.text=@"";
        
        self.confirmPasswordField.placeholder=@"Confirm Password";
        self.confirmPasswordField.text=@"";
    }
    else
    {
        [self callingServiceForgotPassword];
    }
}


-(void)showAlertWithMessage:(NSString *)strmsg
{
    [appDelegate showAlertViewWithMessage:strmsg WithTitle:@"ISO Team" cancelButton:@"OK"];
}


#pragma UITextField Delegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
    
    if (textField.tag==2) {
        
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIToolbar * keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    keyboardToolBar.barStyle = UIBarStyleDefault;
    keyboardToolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)]];
    textField.inputAccessoryView = keyboardToolBar;
}




- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}



-(IBAction)resignKeyboard
{
    [self.registeredMobileNumField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    [self.confirmPasswordField resignFirstResponder];
}



#pragma WebCommunication Delegate Methods

-(void)callingServiceForgotPassword
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl = [NSString stringWithFormat:@"%@%@",BaseURL,UrlSetUserPassword];
    
    NSDictionary *params=@{
                           @"phone":@"9591235924",
                           @"password":self.passwordField.text,
                          };
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"ForgotPwd"];
}


#pragma mark - TTLCommunicationHandlerDelegate methods

- (void)communicationHandler  : (WebCommunicationHandler *)handler
downloadDidCompleteWithResponse: (NSHTTPURLResponse *)serverResponse
                        error : (NSError *) error
              responseObject  : (id) responseObject
                      context : (NSString *) context;
{
    NSLog(@"responseObject:%@",responseObject);
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (responseObject && error == nil)
    {
        if ([@"ForgotPwd"isEqualToString:context])
        {
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                if ([[responseObject valueForKey:@"Status"]isEqualToString:@"Success"])
                {
                    [appDelegate showAlertViewWithMessage:[responseObject valueForKey:@"Password"] WithTitle:@"Overhaul" cancelButton:@"OK"];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else
                {
                    [appDelegate showAlertViewWithMessage:[responseObject valueForKey:@"User"] WithTitle:@"Overhaul" cancelButton:@"OK"];
                }
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
    }
    else
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
