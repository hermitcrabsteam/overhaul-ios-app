//
//  HomeViewController.m
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeCollectionViewCell.h"

@interface HomeViewController ()
{
    AppDelegate *appDelegate;
}

@property (nonatomic,retain) NSArray *arrCategoryItems;
@property (nonatomic,retain) NSArray *arrCategoryTitles;
@property (nonatomic,retain) NSArray *arrCategoryImages, *arrCategoryBackgrdImages;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = kAppDelegate;
    
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
    
    self.arrCategoryItems = @[@"product",@"services",@"news-1",@"healthinfo",@"placeof-interest",@"events",@"music",@"essential"];
    
    self.arrCategoryTitles = @[@"Service & Maintenance",@"Repairs",@"Fuel Stations",@"Spare Parts",@"Accessories",@"Performance Upgrade",@"Car Wash",@"Tyres"];
    
    self.arrCategoryImages = @[@"icon_-_service",@"Repair",@"Fuel_icon",@"Spare_icon",@"Accessories_icon",@"Performance_icon",@"car_wash_icon",@"Tyres_icon"];
    
    self.arrCategoryBackgrdImages = @[@"mazda-engine-sedan-cars-203454",@"Service_Image",@"Layer_10",@"spare",@"Layer_4",@"Speedometer-Macro-Close-Up-Grayscale-Wide-HD-Wallpaper",@"Layer_5",@"Layer_28"];
}



#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    self.latitudeString = @"28.57869";
    self.longitudeString = @"77.339304";
    
    NSLog(@"didFailWithError: %@", error);
}



-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = locations.lastObject;
    
    NSLog(@"-----Current Location----%@", currentLocation);
    
    if (currentLocation != nil)
    {
        self.latitudeString = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        self.longitudeString = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        
        
        NSLog(@"---- Latitude: %@, -----  Longitude: %@", [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude], [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]);
    }
    
    [locationManager stopUpdatingLocation];
}



#pragma mark - UICollectionViewDelgates

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    CGFloat height = CGRectGetHeight(self.homeCollectionView.frame)/4.1f;
    CGFloat width = CGRectGetWidth(self.homeCollectionView.frame)/2.0f;
    
    return CGSizeMake( width-1 , height );
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrCategoryTitles.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"HomeCollectionViewCell";
    
    HomeCollectionViewCell *cell = (HomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.categoryBackgrdImageView.image = [UIImage imageNamed:(self.arrCategoryBackgrdImages)[indexPath.row]];
    cell.categoryBackgrdImageView.alpha = 0.3f;
    
    cell.categoryImageView.image = [UIImage imageNamed:(self.arrCategoryImages)[indexPath.row]];
    cell.categoryLabel.text = (self.arrCategoryTitles)[indexPath.row];
    
    if (IS_IPhone4_HEIGHT_GTE_480)
    {
        (cell.categoryLabel).font = [UIFont systemFontOfSize:12];
    }
    else
    {
        (cell.categoryLabel).font = [UIFont systemFontOfSize:14];
    }
    
    return cell;
}





#pragma mark - Collection view layout things

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(4,0,4,0); // top, left, bottom, right
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{    
    if (indexPath.row == 0)
    {
        [self callingServiceStations];
    }
    else if (indexPath.row == 1)
    {
        [self callingRepairsStations];
    }
    else if (indexPath.row == 2)
    {
        [self callingFuelStations];
    }
    else if (indexPath.row == 3)
    {
        [self callingSparePartsStations];
    }
    else if (indexPath.row == 4)
    {
        [self callingAccessoriesStations];
    }
    else if (indexPath.row == 5)
    {
        [self callingPerformanceUpgradeStations];
    }
    else if (indexPath.row == 6)
    {
        [self callingCarWashStations];
    }
    else
    {
        [self callingTyreStations];
    }
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * deselectedCell = [collectionView cellForItemAtIndexPath:indexPath];
    (deselectedCell.layer).borderColor = [UIColor clearColor].CGColor;
    deselectedCell.layer.borderWidth = 0.0f;
}



-(void)callingServiceStations
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlGetStations];
    
    NSDictionary *params=@{
                           @"latitude" : @"28.6595035",  //self.latitudeString,
                           @"longitude" : @"77.3170602999999",  //self.longitudeString,
                           @"distance" : @"1"
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"ServiceStations"];
}


-(void)callingRepairsStations
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlGetRepairsStations];
    
    NSDictionary *params=@{
                           @"latitude" : @"28.6595035",  //self.latitudeString,
                           @"longitude" : @"77.3170602999999",  //self.longitudeString,
                           @"distance" : @"5"
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"RepairStations"];
}


-(void)callingFuelStations
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlGetFuelStations];
    
    NSDictionary *params=@{
                           @"latitude" : @"28.6595035",  //self.latitudeString,
                           @"longitude" : @"77.3170602999999",  //self.longitudeString,
                           @"distance" : @"5"
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"FuelStations"];
}


-(void)callingSparePartsStations
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlGetSpareParts];
    
    NSDictionary *params=@{
                           @"latitude" : @"28.6595035",  //self.latitudeString,
                           @"longitude" : @"77.3170602999999",  //self.longitudeString,
                           @"distance" : @"5"
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"SparePartsStations"];
}

-(void)callingAccessoriesStations
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlGetAccessories];
    
    NSDictionary *params=@{
                           @"latitude" : @"28.6595035",  //self.latitudeString,
                           @"longitude" : @"77.3170602999999",  //self.longitudeString,
                           @"distance" : @"5"
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"AccessoriesStations"];
}


-(void)callingPerformanceUpgradeStations
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlGetPerformanceUpgrade];
    
    NSDictionary *params=@{
                           @"latitude" : @"28.6595035",  //self.latitudeString,
                           @"longitude" : @"77.3170602999999",  //self.longitudeString,
                           @"distance" : @"5"
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"PerformanceUpgrade"];
}


-(void)callingCarWashStations
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlGetCarWash];
    
    NSDictionary *params=@{
                           @"latitude" : @"28.6595035",  //self.latitudeString,
                           @"longitude" : @"77.3170602999999",  //self.longitudeString,
                           @"distance" : @"5"
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"CarWashStations"];
}


-(void)callingTyreStations
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl=[NSString stringWithFormat:@"%@%@",BaseURL,UrlGetTyres];
    
    NSDictionary *params=@{
                           @"latitude" : @"28.6595035",  //self.latitudeString,
                           @"longitude" : @"77.3170602999999",  //self.longitudeString,
                           @"distance" : @"5"
                           };
    
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"TyreStations"];
}

#pragma WebCommunication Delegate Methods

-(void)communicationHandler:(WebCommunicationHandler *)handler
downloadDidCompleteWithResponse:(NSHTTPURLResponse *)serverResponse
                      error:(NSError *)error
             responseObject:(id)responseObject
                    context:(NSString *)context
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSLog(@"responseObject:%@",responseObject);
    
    if (responseObject &&(error== nil))
    {
        if ([context isEqualToString:@"ServiceStations"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                ServiceListViewController *serviceListInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceListView"];
                
                serviceListInstance.serviceStationsArray = [NSArray arrayWithArray:responseObject[@"ServiceStations"]];
                
                [self.navigationController pushViewController:serviceListInstance animated:YES];
                
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([context isEqualToString:@"RepairStations"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                ServiceListViewController *serviceListInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceListView"];
                
                serviceListInstance.serviceStationsArray = [NSArray arrayWithArray:responseObject[@"RepairsOutletss"]];
                
                [self.navigationController pushViewController:serviceListInstance animated:YES];
                
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([context isEqualToString:@"FuelStations"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                ServiceListViewController *serviceListInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceListView"];
                
                serviceListInstance.serviceStationsArray = [NSArray arrayWithArray:responseObject[@"PetrolBunks"]];
                
                [self.navigationController pushViewController:serviceListInstance animated:YES];
                
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([context isEqualToString:@"SparePartsStations"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                ServiceListViewController *serviceListInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceListView"];
                
                serviceListInstance.serviceStationsArray = [NSArray arrayWithArray:responseObject[@"CarStations"]];
                
                [self.navigationController pushViewController:serviceListInstance animated:YES];
                
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([context isEqualToString:@"AccessoriesStations"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                ServiceListViewController *serviceListInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceListView"];
                
                serviceListInstance.serviceStationsArray = [NSArray arrayWithArray:responseObject[@"AccessoryOutlets"]];
                
                [self.navigationController pushViewController:serviceListInstance animated:YES];
                
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([context isEqualToString:@"PerformanceUpgrade"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                ServiceListViewController *serviceListInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceListView"];
                
                serviceListInstance.serviceStationsArray = [NSArray arrayWithArray:responseObject[@"PerformanceUpgradess"]];
                
                [self.navigationController pushViewController:serviceListInstance animated:YES];
                
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([context isEqualToString:@"CarWashStations"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                ServiceListViewController *serviceListInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceListView"];
                
                serviceListInstance.serviceStationsArray = [NSArray arrayWithArray:responseObject[@"CarStations"]];
                
                [self.navigationController pushViewController:serviceListInstance animated:YES];
                
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
        else if ([context isEqualToString:@"TyreStations"])
        {
            if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
            {
                ServiceListViewController *serviceListInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceListView"];
                
                serviceListInstance.serviceStationsArray = [NSArray arrayWithArray:responseObject[@"TyreOutlets"]];
                
                [self.navigationController pushViewController:serviceListInstance animated:YES];
                
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
    }
    else
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
    }
}




- (IBAction)onClickMenuButton:(id)sender {
    
    MenuViewController *menuInstance =[self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    [self.navigationController pushViewController:menuInstance animated:YES];
}

- (IBAction)onClickRecentlyViewedButton:(id)sender {
    
}

- (IBAction)onClickChatButton:(id)sender {
    
}

- (IBAction)onClickNotificationButton:(id)sender {
    
    NotificationViewController *notificationInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationView"];
    [self.navigationController pushViewController:notificationInstance animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
