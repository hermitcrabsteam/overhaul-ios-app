//
//  HomeViewController.h
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "AppDelegate.h"
#import "WebCommunicationHandler.h"
#import <CoreLocation/CoreLocation.h>

@interface HomeViewController : UIViewController <WebCommunicationHandlerDelegate, CLLocationManagerDelegate>
{
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    CLLocationManager *locationManager;
}

@property CLLocationCoordinate2D center;
@property (strong, nonatomic) NSString *latitudeString, *longitudeString;
@property (strong, nonatomic) IBOutlet UICollectionView *homeCollectionView;

- (IBAction)onClickMenuButton:(id)sender;
- (IBAction)onClickRecentlyViewedButton:(id)sender;
- (IBAction)onClickChatButton:(id)sender;
- (IBAction)onClickNotificationButton:(id)sender;


@end
