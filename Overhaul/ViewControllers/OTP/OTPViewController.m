//
//  OTPViewController.m
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "OTPViewController.h"
#import "SignUpViewController.h"

@interface OTPViewController ()
{
    AppDelegate *appDelegate;
    DHValidation *validation;
    NSMutableCharacterSet *characterSet1;
}

@end

@implementation OTPViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = kAppDelegate;
    
    validation=[[DHValidation alloc]init];
    characterSet1 =[[NSMutableCharacterSet alloc]init];
    [characterSet1 addCharactersInString:@"0123456789"];
    
    if ([self.isComingFrom isEqualToString:@"VerifyMobileNumber"])
    {
        self.mobileNumberLabel.text = [NSString stringWithFormat:@"+91 %@", self.givenMobileNumber];
        
        [self.sendOTPScrollView setHidden:YES];
        [self.sendOTPScrollView setHidden:YES];
        
        [self.verificationScrollView setHidden:NO];
        [self.verificationScrollView setHidden:NO];
    }
    else
    {
        [self.sendOTPScrollView setHidden:NO];
        [self.sendOTPScrollView setHidden:NO];
        
        self.mobileNumberTextField.text = self.givenMobileNumber;
        
        [self.verificationScrollView setHidden:YES];
        [self.verificationScrollView setHidden:YES];
    }
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    
    UITouch *touch = [touches anyObject];
    CGPoint touch_point = [touch locationInView:self.view];
        
    if ([self.view pointInside:touch_point withEvent:event])
    {
        [self resignKeyboard];
    }
}


-(void)resignKeyboard
{
    [self.mobileNumberTextField resignFirstResponder];
    [self.verificationTextField resignFirstResponder];
}



- (IBAction)onClickCloseButton:(id)sender {
    
    SignUpViewController *signUpInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpView"];
    signUpInstance.closeButtonDelegateFrom = @"isFromCloseButton";
    [self.navigationController pushViewController:signUpInstance animated:NO];
    
//    [[self delegate] setCloseButtonString:@"fromCloseButton"];
//    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)onClickSendOTPButton:(id)sender {
    
    if (![validation validateNotEmpty:self.mobileNumberTextField.text])
    {
        [self showAlertWithMessage:@"Please enter the Mobile Number"];
        self.mobileNumberTextField.placeholder=@"Mobile Number";
        self.mobileNumberTextField.text=@"";
    }
    else if (![validation validateStringInCharacterSet:self.mobileNumberTextField.text characterSet:characterSet1])
    {
        [self showAlertWithMessage:@"Please enter valid Mobile Number"];
        self.mobileNumberTextField.placeholder=@"Mobile Number";
        self.mobileNumberTextField.text=@"";
    }
    else
    {
        [self callingServiceForResendOTP];
    }
}


-(void)showAlertWithMessage:(NSString *)strmsg
{
    [appDelegate showAlertViewWithMessage:strmsg WithTitle:@"Overhaul" cancelButton:@"OK"];
}


- (IBAction)onClickResendCode:(id)sender {
    
    self.otpFrom = @"Resend";
    [self callingServiceForResendOTP];
}


- (IBAction)onClickConfirmButton:(id)sender
{
    if (![validation validateNotEmpty:self.verificationTextField.text])
    {
        [self showAlertWithMessage:@"Please enter the Verification Code"];
        self.mobileNumberTextField.placeholder=@"Verification Code";
        self.mobileNumberTextField.text=@"";
    }
    else if (![validation validateStringInCharacterSet:self.verificationTextField.text characterSet:characterSet1])
    {
        [self showAlertWithMessage:@"Please enter the Verification Code"];
        self.mobileNumberTextField.placeholder=@"Verification Code";
        self.mobileNumberTextField.text=@"";
    }
    else
    {
        [self callingServiceForVerifyUser];
        
//        [[self delegate] setConfirmButtonString:@"fromConfirmButton"];
//        [self.navigationController popViewControllerAnimated:YES];
    }
}



#pragma WebCommunication Delegate Methods

-(void)callingServiceForVerifyUser
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl = [NSString stringWithFormat:@"%@%@",BaseURL,UrlVerifyUser];
    
    NSDictionary *params=@{
                           @"phone" : self.givenMobileNumber,
                           @"otp" : self.verificationTextField.text
                           };
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"VerifyUser"];
}


-(void)callingServiceForResendOTP
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl = [NSString stringWithFormat:@"%@%@",BaseURL,UrlResendUserOTP];
    
    NSDictionary *params=@{
                           @"phone" : self.givenMobileNumber
                           };
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"ResendOTP"];
}



#pragma mark - TTLCommunicationHandlerDelegate methods

- (void)communicationHandler  : (WebCommunicationHandler *)handler
downloadDidCompleteWithResponse: (NSHTTPURLResponse *)serverResponse
                        error : (NSError *) error
              responseObject  : (id) responseObject
                      context : (NSString *) context
{
    NSLog(@"responseObject:%@",responseObject);
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (responseObject && error == nil)
    {
        if ([@"VerifyUser"isEqualToString:context])
        {
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                if ([[responseObject valueForKey:@"Status"] isEqualToString:@"Success"])
                {
                    SignUpViewController *signUpInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpView"];
                    signUpInstance.closeButtonDelegateFrom = @"isFromConfirmButton";
                    signUpInstance.otpNumber = self.verificationTextField.text;
                    signUpInstance.eitherOrString = self.isDefinedFrom;
                    signUpInstance.params = self.paramsDict;
                    [self.navigationController pushViewController:signUpInstance animated:NO];
                }
                else
                {
                    [appDelegate showAlertViewWithMessage:[responseObject valueForKey:@"Status"] WithTitle:@"Overhaul" cancelButton:@"OK"];
                }
            }
            else if ([@"ResendOTP"isEqualToString:context])
            {
                if ([responseObject isKindOfClass:[NSDictionary class]])
                {
                    if ([[responseObject valueForKey:@"Status"][@"Status"] isEqualToString:@"Success"])
                    {
                        if ([self.otpFrom isEqualToString:@"Resend"])
                        {
                            
                        }
                        else
                        {
                            [self.sendOTPScrollView setHidden:YES];
                            [self.sendOTPScrollView setHidden:YES];
                            
                            [self.verificationScrollView setHidden:NO];
                            [self.verificationScrollView setHidden:NO];
                        }
                    }
                    else
                    {
                        [appDelegate showAlertViewWithMessage:[responseObject valueForKey:@"Status"][@"Status"] WithTitle:@"Overhaul" cancelButton:@"OK"];
                    }
                }
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
    }
    else
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
