//
//  OTPViewController.h
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebCommunicationHandler.h"
#import "Constant.h"

@protocol verifyPassword;

@interface OTPViewController : UIViewController <WebCommunicationHandlerDelegate>

@property (retain)id <verifyPassword> delegate;
@property (strong, nonatomic) NSString *otpFrom, *isDefinedFrom;
@property (strong, nonatomic) NSDictionary *paramsDict;
@property (strong, nonatomic) NSString *isComingFrom, *givenMobileNumber;
@property (strong, nonatomic) IBOutlet UILabel *mobileNumberLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *sendOTPScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *verificationScrollView;
@property (strong, nonatomic) IBOutlet UITextField *mobileNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *verificationTextField;
@property (strong, nonatomic) IBOutlet UIButton *resendCodeButton;

- (IBAction)onClickCloseButton:(id)sender;
- (IBAction)onClickSendOTPButton:(id)sender;
- (IBAction)onClickResendCode:(id)sender;
- (IBAction)onClickConfirmButton:(id)sender;

@end


@protocol verifyPassword <NSObject>

-(void) setCloseButtonString:(NSString *) isFrom;
-(void) setConfirmButtonString:(NSString *) isFrom;

@end
