//
//  MenuViewController.h
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface MenuViewController : UIViewController <WebCommunicationHandlerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *menuTableView;

- (IBAction)onClickLogoutButton:(id)sender;
- (IBAction)onClickSettingsButton:(id)sender;

@end
