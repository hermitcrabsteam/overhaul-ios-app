//
//  MenuViewController.m
//  Overhaul
//
//  Created by admin on 07/07/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuListTableViewCell.h"
#import "MenuUserTableViewCell.h"
#import "HomeViewController.h"
#import "SettingsViewController.h"

@interface MenuViewController ()
{
    AppDelegate *appDelegate;
}

@property (nonatomic,retain) NSArray *arrayMenuOptions;
@property (nonatomic,retain) NSArray *arrayMenuIcons;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.arrayMenuOptions = [[NSMutableArray alloc] initWithObjects:@"Home",@"My Profile",@"News & Updates",@"Car Reviews",@"Favourites",@"Contact Us",@"About Us",@"Rate Us",@"Invite Friends", nil];
    
    self.arrayMenuIcons = [[NSMutableArray alloc] initWithObjects:@"_about_us",@"icon_-_my_Profile_",@"News_icon",@"icon car reviews",@"icon_-_Favorites",@"call_icon",@"icon_-_about_us",@"star_icon",@"icon_-_invite_friends",nil];
}


- (IBAction)onClickLogoutButton:(id)sender {
    
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Overhaul" message:@"Are You Sure, Do you want to Logout ?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"No", @"Yes", nil];
    
    myAlert.tag = 1;
    [myAlert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        
        if (buttonIndex == 0)
        {
            
        }
        else
        {
            // [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"LoginID"];
            
            ViewController *viewInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
            [self.navigationController pushViewController:viewInstance animated:YES];
        }
    }
    else
    {
        if (buttonIndex == 0) {
            
        }
        else if (buttonIndex == 1) {
            
        }
        else
        {
            
        }
    }
    
}


- (IBAction)onClickSettingsButton:(id)sender {
    
    SettingsViewController *viewInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsView"];
    [self.navigationController pushViewController:viewInstance animated:YES];
}



#pragma UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0)
    {
        return 1;
    }
    else
    return self.arrayMenuOptions.count;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            return 200.0;
        }
    }
    else
    {
        return 60;
    }
    
    return 0;
}



- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",(int)indexPath.row);
    
    if (indexPath.section == 0)
    {
        return NO;
    }
    else
    
    return YES;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.separatorColor = [UIColor clearColor];
    
    if (indexPath.section == 0)
    {
        MenuUserTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:@"MenuUserTableViewCell"];
        
        NSString *imageStr = @"https://lh6.googleusercontent.com/--b5ot-3ALkg/AAAAAAAAAAI/AAAAAAAAAGc/QI3bSA7g1-4/s2000/photo.jpg";
        
        [headerView.userImageView setImageWithURL:[NSURL URLWithString:imageStr]];
        
        headerView.userImageView.layer.cornerRadius = headerView.userImageView.frame.size.height /2;
        headerView.userImageView.clipsToBounds = YES;
        
        
        [headerView.closeButton addTarget:self action:@selector(onClickCloseButton) forControlEvents:UIControlEventTouchUpInside];
        
        return headerView;
    }
    else
    {
        MenuListTableViewCell *menuCell = (MenuListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MenuListTableViewCell"];
                
        (menuCell.menuListNameLabel).text = [NSString stringWithFormat:@" %@",(self.arrayMenuOptions)[indexPath.row]];
        (menuCell.menuListIconImageView).image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",(self.arrayMenuIcons)[indexPath.row]]];
        
        menuCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return menuCell;
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        
    }
    else
    {
        int index = (int) indexPath.row;
        
        switch (index)
        {
            case 0:
            {
                HomeViewController *homeInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
                [self.navigationController pushViewController:homeInstance animated:YES];
                
                break;
                
            }
            case 1:
            {
                MyProfileViewController *myProfileInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileView"];
                [self.navigationController pushViewController:myProfileInstance animated:YES];
                
                break;
                
            }
            case 2:
            {
                NewsUpdatesViewController *newsUpdatesInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsUpdatesView"];
                [self.navigationController pushViewController:newsUpdatesInstance animated:YES];
                
                break;
                
            }
            case 3:
            {
                ReviewsViewController *reviewInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewsView"];
                [self.navigationController pushViewController:reviewInstance animated:YES];
                
                break;
            }
            case 4:
            {
                [self callingServiceForGetFavourites];
                
                break;
            }
                
            case 5:
            {
                ContactUsViewController *contactInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsView"];
                [self.navigationController pushViewController:contactInstance animated:YES];
               
                break;
            }
                
            case 6:
            {
                AboutUsViewController *aboutInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsView"];
                [self.navigationController pushViewController:aboutInstance animated:YES];
                
                break;
            }
                
            case 7:
            {
//                RateUsViewController *rateInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"RateUsView"];
//                [self.navigationController pushViewController:rateInstance animated:YES];
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rate Overhaul"
                                                                message:@"If you enjoy using Overhaul, would you mind taking moment to rate it? Thanks for your support!"
                                                               delegate:self
                                                      cancelButtonTitle:@"No,Thanks"
                                                      otherButtonTitles:@"Rate Overhaul", @"Remind me later", nil];
                alert.tag = 2;
                [alert show];

                break;
            }
                
            case 8:
            {

                NSString *textToShare = @"Look at this awesome website for Apple Developer Program!";
                NSURL *myWebsite = [NSURL URLWithString:@"http://www.apple.com/"];
                
                NSArray *objectsToShare = @[textToShare, myWebsite];
                
                UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
                
                NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                               UIActivityTypePrint,
                                               UIActivityTypeAssignToContact,
                                               UIActivityTypeSaveToCameraRoll,
                                               UIActivityTypeAddToReadingList,
                                               UIActivityTypePostToFlickr,
                                               UIActivityTypePostToVimeo];
                
                activityVC.excludedActivityTypes = excludeActivities;
                
                [self presentViewController:activityVC animated:YES completion:nil];
                
                break;
            }
  
            default:
                break;
        }
    }
}



#pragma WebCommunication Delegate Methods

-(void)callingServiceForGetFavourites
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *strbaseUrl = [NSString stringWithFormat:@"%@%@",BaseURL,UrlGetFavourites];
    
    NSDictionary *params=@{
                           @"userId" : @"1"
                           };
    
    WebCommunicationHandler *communicationHandler = [[WebCommunicationHandler alloc]init];
    [communicationHandler downloadWithURL:strbaseUrl
                                   params:params
                        WithRequestMethod:@"POST"
                                authorize:NO
                           networkHandler:self
                                  context:@"GetFav"];
}


#pragma mark - TTLCommunicationHandlerDelegate methods

- (void)communicationHandler  : (WebCommunicationHandler *)handler
downloadDidCompleteWithResponse: (NSHTTPURLResponse *)serverResponse
                        error : (NSError *) error
              responseObject  : (id) responseObject
                      context : (NSString *) context;
{
    NSLog(@"responseObject:%@",responseObject);
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (responseObject && error == nil)
    {
        if ([@"GetFav"isEqualToString:context])
        {
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                if ([[responseObject valueForKey:@"Status"]isEqualToString:@"Success"])
                {                    
                    FavouritesViewController *favView = [self.storyboard instantiateViewControllerWithIdentifier:@"FavouriteView"];
                    
                    favView.favouritesArray = responseObject[@"Favourites"];
                    
                    [self.navigationController pushViewController:favView animated:YES];
                }
                else
                {
                    AddVehicleViewController *addVehicleInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"AddVehicleView"];
                    [self.navigationController pushViewController:addVehicleInstance animated:YES];
                    
                    [appDelegate showAlertViewWithMessage:[responseObject valueForKey:@"User"] WithTitle:@"Overhaul" cancelButton:@"OK"];
                }
            }
            else
            {
                [appDelegate showAlertViewWithMessage:@"Result Failed, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
            }
        }
    }
    else
    {
        [appDelegate showAlertViewWithMessage:@"Network Issue, Please try again" WithTitle:@"Overhaul" cancelButton:@"OK"];
    }
}



-(void) onClickCloseButton
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
