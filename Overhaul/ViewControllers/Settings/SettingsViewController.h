//
//  SettingsViewController.h
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface SettingsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *settingsTableView;

- (IBAction)onClickBackButton:(id)sender;
- (IBAction)onClickNotificationButton:(id)sender;

@end
