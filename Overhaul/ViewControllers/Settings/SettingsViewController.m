//
//  SettingsViewController.m
//  Overhaul
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 hermitcrabs. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsNotificationTableViewCell.h"
#import "SettingsPwdTableViewCell.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



#pragma UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}



//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 35;
//}



- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",(int)indexPath.row);
    
//    if (indexPath.section == 0) {
//        
//        return NO;
//    }
//    else
    return NO;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        SettingsNotificationTableViewCell *notificationCell = [tableView dequeueReusableCellWithIdentifier:@"SettingsNotificationTableViewCell"];
        
        [notificationCell.toggleButton addTarget:self action:@selector(onClickToggleButton) forControlEvents:UIControlEventTouchUpInside];
        
        return notificationCell;
    }
    else
    {
        SettingsPwdTableViewCell *pwdCell = [tableView dequeueReusableCellWithIdentifier:@"SettingsPwdTableViewCell"];
        
        [pwdCell.changePwdButton addTarget:self action:@selector(onClickChangePWDButton) forControlEvents:UIControlEventTouchUpInside];
        
        return pwdCell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void) onClickChangePWDButton
{
    
}

-(void) onClickToggleButton:(UIButton *) sender
{
    
}


- (IBAction)onClickBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickNotificationButton:(id)sender {
    
    NotificationViewController *notificationInstance = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationView"];
    [self.navigationController pushViewController:notificationInstance animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
