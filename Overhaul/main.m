//
//  main.m
//  Overhaul
//
//  Created by Kumar m p, Hemanth - Hemanth on 11/9/16.
//  Copyright © 2016 Kumar m p, Hemanth - Hemanth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
